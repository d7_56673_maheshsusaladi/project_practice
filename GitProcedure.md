## Steps for Git in project 

### Step 1: Initialize git repository
```bash
> git init
```
### Step 2: Create local copy of git repository
```bash
> git clone <url>
```
### Step 3: Switch branch from main to user created branch 
```bash
> git checkout -b <branch_name>
```
### Step 4: Do the changes in project and add them
```bash
> git add .
```
### Step 5: Commit the changes 
```bash
> git commit -m  "Commit message"
```
### Step 6: Switch branch to main from user created branch 
```bash
> git checkout main
```
### Step 7: Merge the user created branch in main branch 
```bash
> git merge <branch_name>
```
### Step 8: Push the changes into the remote repository 
```bash
> git push
```
### Step 9 : Delete user created branch
```bash
> git branch -d <branch_name>
```
-------------------------------------
## Steps for updated git repository

### Step 1: Switch to main branch ( if not )
```
> git checkout main
```
### Step 2: pull git repository
```bash
> git pull
```
- Update the maven project (as shown in dia.), so that changes will be reflected in your project.
- After above step 2, follow steps from step 3 to step 8.