import "./index.css";
import { useLocation, useNavigate } from "react-router";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import DaywiseScheduleAdd from "../../components/DaywiseScheduleAdd";
const AddNewPackage = () => {
  const { state } = useLocation();
  const { id } = state;
  const [packageName, setPackageName] = useState("");
  const [pricePerPerson, setPricePerPerson] = useState("");
  const [category, setCategory] = useState("");
  const [startDate, setStartDate] = useState("");
  const [days, setDays] = useState("");
  const [transportMode, setTransportMode] = useState("");
  const [pkgImage, setPkgImage] = useState("");
  const [newpackage, setNewPackage] = useState("");
  const [imageFile, setImageFile] = useState("");
  const destinationId = id;

  const navigate = useNavigate();

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const AddPackage = () => {
    if (packageName.length === 0) {
      toast.warning("Please Enter Package Name");
    } else if (pricePerPerson.length === 0) {
      toast.warning("Please Enter Price per person");
    } else if (category.length === 0) {
      toast.warning("Select category");
    } else if (transportMode.length === 0) {
      toast.warning("Select transportMode");
    } else {
      const body = {
        packageName,
        pricePerPerson,
        category,
        startDate,
        days,
        transportMode,
        pkgImage,
        destinationId,
      };

      authAxios
        .post("http://localhost:8080/admin/add_package", body)
        .then((response) => {
          const result = response.data;
          if (result.status === "success") {
            console.log(result.data);
            setNewPackage(result.data);
            setDays(newpackage.days);
          } else {
            toast.error(`${packageName} not added...`);
          }
        });
    }
  };

  const arr = [];
  // create array for number of days
  for (let i = 1; i <= newpackage.days; i++) {
    arr.push(i);
  }

  sessionStorage.setItem("packageId", newpackage.packageId);

  // File upload code
  const uploadFile = () => {
    const formdata = new FormData();
    formdata.append("datafile", imageFile);

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    let url = `http://localhost:8080/admin/image/upload/package/${newpackage.packageId}`;
    console.log(imageFile);
    console.log(url);
    authAxios.post(url, formdata, config).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        toast.success("File uploaded successfully....");
        navigate("/destinations");
      }
    });
  };

  return (
    <div className="imagebg1">
      <br></br>
      <div className="row g-5" style={{ padding: "10px" }}>
        <div className="col-md-4">
          <div className="form-control transferboxpkg">
            <h2>Add New Package</h2>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                Package Name
              </label>
              <input
                onChange={(e) => {
                  setPackageName(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                Price/Person
              </label>
              <input
                onChange={(e) => {
                  setPricePerPerson(e.target.value);
                }}
                type="number"
                min={500}
                max={20000}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                Category
              </label>
              <select
                className="form-select"
                id="inputGroupSelect01"
                onChange={(e) => {
                  setCategory(e.target.value);
                }}
              >
                <option value="">Choose...</option>
                <option value="Group">Group</option>
                <option value="Solo">Solo</option>
                <option value="Couple">Couple</option>
                <option value="Family">Family</option>
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                Start Date
              </label>
              <input
                onChange={(e) => {
                  setStartDate(e.target.value);
                }}
                type="date"
                pattern="yyyy-mm-dd"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                No of Days
              </label>
              <input
                onChange={(e) => {
                  setDays(e.target.value);
                }}
                type="number"
                min={1}
                max={10}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-pkg">
                Transport Mode
              </label>
              <select
                className="form-select"
                id="inputGroupSelect01"
                onChange={(e) => {
                  setTransportMode(e.target.value);
                }}
              >
                <option value="">Choose....</option>
                <option value="BUS">Bus</option>
                <option value="TRAIN">Train</option>
                <option value="FLIGHT">Flight</option>
                <option value="SHIP">Ship</option>
              </select>
            </div>
            <div className="row">
              <div className="col">
                <button
                  onClick={AddPackage}
                  type="button"
                  class="btn btn-primary"
                >
                  Add Day-wise Schedule
                </button>
              </div>
              <div className="col"></div>
            </div>
          </div>
        </div>

        <div className="col-md-7 ">
          <div>
            {newpackage.length !== 0 && (
              <div className="row g-4">
                <div className="transferboxpkg">
                  <div class=" p-2 mb-4  rounded ">
                    <table className="table  table-hover ">
                      <thead>
                        <th>Day</th>
                        <th>Agenda</th>
                        <th>Description</th>
                        <th>Action</th>
                        <th>BreakFast</th>
                        <th>Lunch</th>
                        <th>Dinner</th>
                      </thead>
                      <tbody>
                        {arr.map((i) => {
                          return (
                            <DaywiseScheduleAdd num={i}></DaywiseScheduleAdd>
                          );
                        })}
                      </tbody>
                    </table>
                    <div className="mb-3 ">
                      <label className="label-control-pkg ">
                        Upload Package Image
                      </label>
                      <input
                        type="file"
                        className="form-control"
                        accept=".jpg/.jpeg/.png"
                        onChange={(e) => {
                          setImageFile(e.target.files[0]);
                        }}
                      />
                    </div>
                    <div className="mb-3 ">
                      <button onClick={uploadFile} className="btn btn-primary">
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddNewPackage;
