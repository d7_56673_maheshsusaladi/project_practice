import "./index.css";
import { useNavigate } from "react-router";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
const AddNewDestination = () => {
  const [countryName, setCountryName] = useState("");
  const [countryInfo, setCountryInfo] = useState("");
  const [stateName, setStateName] = useState("");
  const [stateInfo, setStateInfo] = useState("");
  const [imageFile, setImageFile] = useState();
  const destinationId = 0;
  const [destination, setDestination] = useState("");
  const [enabled, setEnabled] = useState(true);
  const navigate = useNavigate();

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const AddDest = () => {
    if (countryInfo.length === 0) {
      toast.warning("Please enter CountryInfo");
    } else if (stateInfo.length === 0) {
      toast.warning("Please enter StateInfo");
    } else {
      const body = {
        destinationId,
        countryName,
        countryInfo,
        stateName,
        stateInfo,
      };
      console.log(body);

      authAxios
        .post("http://localhost:8080/admin/add_destination", body)
        .then((response) => {
          const result = response.data;
          if (result.status === "success") {
            // navigate("/destinations");
            toast.success(`${stateName} info added successfully`);
            setDestination(result.data);
            console.log(result.data);
            setEnabled(false);
          } else if (response.status == 403)
          {
            toast.error(`Access denied for unauthorized personnel`);
           
          }
          else
          {
            toast.error(`${stateName} not added`);
          }
        });
    }
  };

  // File upload code
  const uploadFile = () => {
    const formdata = new FormData();
    formdata.append("datafile", imageFile);

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    let url = `http://localhost:8080/admin/image/upload/${destination.destinationId}`;
    console.log(imageFile);
    console.log(url);
    authAxios.post(url, formdata, config).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        toast.success("File uploaded successfully....");
        navigate("/destinations");
      }
    });
  };

  return (
    <div className="imagebg">
      <br></br>

      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div
            className="shadow p-3 bg-body rounded "
            style={{ marginBottom: "20px" }}
          >
            <h1>Add New Destination</h1>
            <div className="form ">
              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  Country Name
                </label>

                <input
                  onChange={(e) => {
                    setCountryName(e.target.value);
                  }}
                  type="text"
                  className="form-control"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  Country Info
                </label>
                <textarea
                  onChange={(e) => {
                    setCountryInfo(e.target.value);
                  }}
                  className="form-control"
                ></textarea>
              </div>

              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  State Name
                </label>
                <input
                  onChange={(e) => {
                    setStateName(e.target.value);
                  }}
                  type="text"
                  className="form-control"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  State Info
                </label>
                <textarea
                  onChange={(e) => {
                    setStateInfo(e.target.value);
                  }}
                  className="form-control"
                ></textarea>
              </div>
              <button
                disabled={!enabled}
                onClick={AddDest}
                className="btn btn-primary"
              >
                Save
              </button>
              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  Destination Image
                </label>
                <input
                  type="file"
                  className="form-control"
                  disabled={enabled}
                  onChange={(e) => {
                    setImageFile(e.target.files[0]);
                  }}
                />
              </div>
              <button
                disabled={enabled}
                onClick={uploadFile}
                className="btn btn-primary"
              >
                Upload Image
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
};

export default AddNewDestination;
