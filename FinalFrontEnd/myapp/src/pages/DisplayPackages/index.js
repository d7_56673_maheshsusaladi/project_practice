import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import axios from "axios";
import "../../components/styles.css";
import NavBar from "../../components/NarBar";
import {
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Button,
  Col,
  Row,
  Container,
} from "reactstrap";
const DisplayPackages = () => {
  const { state } = useLocation();
  const { id } = state;
  const [packages, setPackages] = useState([]);
  const navigate = useNavigate();

  // Checking login status
  const isLoggedUser = sessionStorage.getItem("loginStatus");
  const getAllPackages = () => {
    const url = `http://localhost:8080/home/destination/${id}`;
    axios.get(url).then((response) => {
      const result = response.data;
      console.log(url);
      if (result.status === "success") {
        setPackages(result.data);
        console.log(result.data);
      } else {
        console.log("Error in fetching the data..");
      }
    });
  };

  useEffect(() => {
    getAllPackages();
  }, []);

  return (
    <div>
      <div>
        <NavBar></NavBar>
      </div>
      <Container>
        <Row>
          {packages.map((pkg) => {
            let imageUrl = `http://localhost:8080/home/image/download/package/${pkg.packageId}`;
            return (
              <Card
                className="shadow p-2 mb-3 bg-white rounded cardPopup"
                style={{ width: "320px", margin: "30px" }}
              >
                <CardImg
                  className="card-img-top "
                  style={{ width: "300px", height: "200px" }}
                  src={imageUrl}
                  alt={pkg.packageName}
                />
                <CardBody>
                  <CardTitle>
                    <b>{pkg.packageName}</b>
                  </CardTitle>
                  <CardSubtitle>Start Date: {pkg.startDate}</CardSubtitle>
                  <CardText>Category: {pkg.category}</CardText>
                  <div className="row g-4">
                    <div className="col">
                      <button
                        onClick={() => {
                          navigate("/viewDetails", {
                            state: { pkg: pkg },
                          });
                        }}
                        className="btn btn-danger"
                      >
                        View Details
                      </button>
                    </div>
                    {!isLoggedUser && (
                      <div className="col">
                        <button
                          className="btn btn-success"
                          onClick={() => {
                            alert(`For ${pkg.packageName}, please Contact us on
                        Mobile : +91 9988978765
                        Email : punetours@gmail.com`);
                          }}
                        >
                          Enquiry
                        </button>
                      </div>
                    )}
                    {isLoggedUser && (
                      <div className="col">
                        <button
                          onClick={() => {
                            navigate("/book", { state: { id: pkg.packageId } });
                          }}
                          className="btn btn-primary"
                        >
                          Book Now
                        </button>
                      </div>
                    )}
                  </div>
                </CardBody>
              </Card>
            );
          })}
        </Row>
      </Container>
    </div>
  );
};
export default DisplayPackages;
