import { useLocation } from "react-router";
import axios from "axios";
import { useEffect, useState } from "react";
const UpdateSchedule = () => {
  const { state } = useLocation();
  const { packages } = state;
  const [agenda, setAgenda] = useState("");
  const [description, setDescription] = useState("");
  const [enable, setEnable] = useState(false);
  const [schedule, setSchedule] = useState([]);
  console.log(packages);
  useEffect(() => {
    axios
      .get(`http://localhost:8080/admin/package/schedule/${packages.packageId}`)
      .then((response) => {
        const result = response.data;
        console.log(result.data);
        setSchedule(result.data);
      });
  }, []);

  const arr = [];
  for (let i = 1; i <= packages.days; i++) {
    arr.push(i);
  }
  return (
    <div>
      <div className="container" style={{ backgroundColor: "whiteSmoke" }}>
        <h1>{packages.packageName}</h1>
        <table className="table table-bordered">
          <thead>
            <th scope="col-md-2">Day No</th>
            <th scope="col-md-4">Agenda</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
          </thead>
          <tbody>
            {schedule.map((s) => {
              return (
                <tr>
                  <td>{s.scheduleId}</td>
                  <td>
                    <input
                      value={s.agenda}
                      onChange={(e) => {
                        setAgenda(e.target.value);
                      }}
                      // type="text"
                      // className="form-control"
                    ></input>
                  </td>
                  <td>
                    <textarea
                      cols="50"
                      rows="5"
                      value={s.description}
                      onChange={(e) => {
                        setDescription(e.target.value);
                      }}
                    ></textarea>
                  </td>
                  <td>
                    <button cols="50" rows="5" className="btn btn-primary">
                      Edit
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default UpdateSchedule;
