import { useState, useEffect } from "react";
//import 'bootstrap/dist/css/bootstrap.min.css';

import Carousel from "react-bootstrap/Carousel";
import axios from "axios";
import DestCard from "../../components/HomePageCard";
import { Container, Row } from "reactstrap";
import NavBar from "../../components/NarBar";
const Home = () => {
  const [destinationsIndia, setDestinationsIndia] = useState([]);
  const [destinationsforeign, setDestinationsForeign] = useState([]);
 
  useEffect(() => {
    axios
      .get("http://localhost:8080/home/destinations/India")
      .then((response) => {
        const result = response.data;
        console.log(result.data);
        setDestinationsIndia(result.data);
      });
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:8080/home/destinations/USA")
      .then((response) => {
        const result = response.data;
        setDestinationsForeign(result.data);
      });
  }, []);

  return (
    <div>
      <div>
        <NavBar></NavBar>
      </div>
      <Carousel fade>
        <Carousel.Item>
          <img
            className=" d-block w-100"
            src="https://image.kesari.in/upload/web_banner/SUMMER-BONANZA-web-Banner.jpg"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://image.kesari.in/upload/web_banner/Last-Minute-Deals-April.jpg"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://image.kesari.in/upload/web_banner/Himalaya-Utsav.jpg"
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
      <br></br>
      <div class="card-group ">
        <div
          style={{
            backgroundColor: "gray",
            width: "100%",
            textAlign: "center",
            color: "white",
          }}
        >
          <h3>Indian Tours</h3>
        </div>
        <Row>
          {destinationsIndia.map((destination) => {
            return <DestCard destination={destination}></DestCard>;
          })}
        </Row>
        <div
          style={{
            backgroundColor: "gray",
            width: "100%",
            textAlign: "center",
            color: "white",
          }}
        >
          <h3>Foreign Tours</h3>
        </div>
        <Row>
          {destinationsforeign.map((destination) => {
            return <DestCard destination={destination}></DestCard>;
          })}
        </Row>
      </div>
    </div>
  );
};

export default Home;
