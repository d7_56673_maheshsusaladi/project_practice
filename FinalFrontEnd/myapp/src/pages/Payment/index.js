import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import { Navigate, useLocation } from "react-router";
import { useNavigate } from "react-router";
// import Pay from '../../components/paymentComponent'
import payImg from "../../assets/payment.png";
const Payment = () => {
  const { state } = useLocation();
  const [persons, setPersons] = useState([]);
  // const packageId={state}
  const navigate = useNavigate();
  const { body } = state;
  console.log(body);
  sessionStorage["pricePerPerson"] = body.pricePerPerson;
  const firstName = sessionStorage["firstName"];
  const lastName = sessionStorage["lastName"];
  const email = sessionStorage["email"];
  const mobileNo = sessionStorage["mobileNO"];
  const [total, setTotal] = useState("");
  const [show, setShow] = useState(false);

  // Role is user -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    const person = getPersons();
    const price = getPrice();
  };
  const getPersons = () => {
    const url = `http://localhost:8080/user/booking/getpersons/${body.bookingId}`;
    authAxios.get(url).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        console.log(result.data);
        setPersons(result.data);
      }
    });
  };
  const getPrice = () => {
    authAxios
      .get(`http://localhost:8080/user/booking/getprice/${body.bookingId}`)
      .then((response) => {
        const result = response.data;
        if (result.status === "success") {
          console.log(result.data);
          setTotal(result.data);
        }
      });
  };
  const styles = {
    container: {
      backgroundColor: "white",
      height: "600px",
      width: "600px",
      marginTop: "2%",
      marginBottom: "2%",
    },
  };
  const deleteBooking = () => {
    authAxios
      .delete(`http://localhost:8080/user/booking/${body.bookingId}`)
      .then((response) => {
        const result = response.data;
        if (result.status === "success") {
          console.log(result.data);
          toast.error("Booking canceled !");
          navigate("/home");
        }
      });
  };
  return (
    <div>
      <div className="wrapper">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-3"></div>
            <div className="col-md-6">
              <div className="card cotent shadow-lg mt-2 mb-2 bg-white rounded">
                <div className="class-header"></div>
                <div className="class-body" style={{ padding: "15px" }}>
                  <div className="row form-group">
                    <div className="col-12">
                      <center>
                        <img src={payImg}></img>
                      </center>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-12">
                      <center>
                        <h4> BOOKING CONFIRMED!</h4>
                      </center>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-12">
                      <table className="table table-striped ">
                        <thead>
                          <tr className="bg-info">
                            <td>BOOKING ID:</td>
                            <td></td>
                            <td>
                              <strong>{body.bookingId}</strong>
                            </td>
                            <td>COUNTRY:</td>
                            <td></td>
                            <td>
                              <strong>INDIA</strong>
                            </td>
                          </tr>
                        </thead>
                        <tbody className="col-10">
                          <tr>
                            <td>USER NAME:</td>
                            <td></td>
                            <td>
                              <strong>
                                {firstName} {lastName}
                              </strong>
                            </td>
                            <td>EMAIL:</td>
                            <td></td>
                            <td>
                              <strong>{email}</strong>
                            </td>
                          </tr>

                          <tr>
                            <td>NO OF PERSONS:</td>
                            <td></td>
                            <td>
                              <strong>{persons.length}</strong>
                            </td>
                            <td>PACKAGE NAME:</td>
                            <td></td>
                            <td>
                              <strong>{body.packageName}</strong>
                            </td>
                          </tr>
                          {persons.map((person) => {
                            return (
                              <tr>
                                <td>AGE:</td>
                                <td></td>
                                <td>
                                  <strong>{person.age}</strong>
                                </td>
                                <td>NAME:</td>
                                <td></td>
                                <td>
                                  <strong>{person.name}</strong>
                                </td>
                              </tr>
                            );
                          })}
                          <tr>
                            <td>PRICE PER PERSON</td>
                            <td></td>
                            <td>
                              <strong>{body.pricePerPerson}</strong>
                            </td>
                            <td>TOTAL PRICE</td>
                            <td></td>
                            <td>
                              <strong>{total}</strong>
                            </td>
                          </tr>
                          <tr>
                            <td>START DATE:</td>
                            <td></td>
                            <td>{body.startDate}</td>
                            <td>DAYS:</td>
                            <td></td>
                            <td>{body.days}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="row form-group">
                      <div className="col-6">
                        <p>
                          <strong>CONTACT US</strong>
                          <br />
                          <div>
                            Domestic Tours: 02221234
                            <br />
                            Foreign Tours : +91565668558
                          </div>
                        </p>
                      </div>
                      <div className="col-6">
                        <p>
                          <strong>OFFICE ADDRESS</strong>
                          <br />
                          <div>
                            <strong>Address 1</strong> : sunbeam tours &
                            travels, hinjewadi phase 3,pune 413004
                            <br />
                            <strong>Address 2</strong> : market yard, pune
                          </div>
                        </p>
                      </div>
                    </div>
                    <div className=" row form-group">
                      <div className="col-12">
                        <p>
                          <div>
                            <strong>cancellation policy:</strong>
                            <div>
                              Please note that once you have booked an package
                              with us it means that we have reserved slot in our
                              group tour exclusively for you. If you cancel your
                              booking less than [24 hours] before it is start
                              date, you will be subject to a
                              [penalty/fee/rebooking charge of 70% ].
                            </div>
                            <br />
                            <div>
                              To avoid a cancellation fee, please provide
                              cancellation notice at least [2 days] prior to
                              your booking date.
                            </div>
                            <br />
                            <div>
                              You can cancel or reschedule an appointment by
                              emailing us at [sunbeamTours@sunbeam.com], texting
                              [8275916161], or calling our office at [02221234].
                            </div>
                          </div>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <button
                        hidden={show}
                        className="btn btn-primary"
                        onClick={() => {
                          setShow(true);
                          toast.success("BOOKING SUCCESSFUL!");
                        }}
                      >
                        MAKE PAYEMENT
                      </button>
                    </div>
                    <div className="col">
                      <button
                        hidden={show}
                        className="btn btn-danger"
                        onClick={deleteBooking}
                      >
                        CANCEL BOOKING
                      </button>
                    </div>
                    <div className="col">
                      <div className="col">
                        {show && (
                          <button
                            className="btn btn-primary"
                            onClick={() => {
                              window.print();
                            }}
                          >
                            PRINT INVOICE
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-footer">
                  <div className="footer-copyright">
                    &copy;2022 Sunbeam Tours & Travels
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
