//import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import { useLocation, useNavigate } from "react-router";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
const EditPackage = () => {
  const { state } = useLocation();
  const { id } = state;

  const [packageName, setPackageName] = useState("");
  const [pricePerPerson, setPricePerPerson] = useState("");
  const [category, setCategory] = useState("");
  const [startDate, setStartDate] = useState("");
  const [days, setDays] = useState("");
  const [transportMode, setTransportMode] = useState("");
  const [pkgImage, setPkgImage] = useState("");
  const packageId = id;
  const [show, setShow] = useState(true);
  const [imageFile, setImageFile] = useState("");
  const navigate = useNavigate();

  // Role is admin ->
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });



  const EditDest = () => {
    if (packageName.length === 0) {
      toast.warning("Please enter Package Name");
    } else if (pricePerPerson.length === 0) {
      toast.warning("Please enter Price Per Person");
    } else if (days.length === 0) {
      toast.warning("Please enter Number of Days");
    } else if (imageFile.length === 0) {
      toast.warning("Please choose image");
    } else {
      const body = {
        packageId,
        packageName,
        pricePerPerson,
        category,
        startDate,
        days,
        transportMode,
      };
      console.log(body);
      authAxios
        .post(`http://localhost:8080/admin/edit-package`, body)
        .then((response) => {
          const result = response.data;
          if (result.status === "success") {
            toast.success(`${packageName} updated successfully...`);
            uploadFile();
          } else {
            toast.error(`${packageName} not updated...`);
          }
        });
    }
  };
  // get package details
  const getdetails = () => {
    const url = `http://localhost:8080/admin/getpackage/${id}`;
    authAxios.get(url).then((response) => {
      const result = response.data;
      console.log(result.data);
      setPackageName(result.data.packageName);
      setPricePerPerson(result.data.pricePerPerson);
      setCategory(result.data.category);
      setStartDate(result.data.startDate);
      setDays(result.data.days);
      setTransportMode(result.data.transportMode);
      setShow(false);
    });
  };
  let imageUrl = `http://localhost:8080/home/image/download/package/${id}`;
  // File upload code
  const uploadFile = () => {
    const formdata = new FormData();
    formdata.append("datafile", imageFile);
    console.log(imageFile);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    let url = `http://localhost:8080/admin/image/upload/package/${id}`;
    console.log(url);
    authAxios.post(url, formdata, config).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        toast.success("File uploaded successfully....");
        navigate("/destinations", { state: { id: id } });
      }
    });
  };

  return (
    <div id="bottomContainer" >
      <div className="row">
        <div className="col"></div>
        <div className="col-md-6" style={{ padding: "10px" }}>
          <div className="form-control transferboxeditpkg" >
            <h2>{packageName}</h2>
            {{ days } === "0" ? (
              <img
                style={{ width: "50%", height: "50%" }}
                src="https://cdn.pixabay.com/photo/2015/04/19/08/33/flower-729512__340.jpg"
                alt={packageName}
              ></img>
            ) : (
              <img
                style={{ width: "50%", height: "50%" }}
                src={imageUrl}
                alt={packageName}
              ></img>
            )}
            <div className="row">
              <div className="col">
                <label htmlFor="" className="label-controleEditpkg">
                  Package Name
                </label>
                <input
                  value={packageName}
                  readOnly={true}
                  onChange={(e) => {
                    setPackageName(e.target.value);
                  }}
                  type="text"
                  className="form-control"
                />
              </div>
              <div className="col">
                <label htmlFor="" className="label-controleEditpkg">
                  Price/Person
                </label>
                <input
                  value={pricePerPerson}
                  onChange={(e) => {
                    setPricePerPerson(e.target.value);
                  }}
                  type="number"
                  min={500}
                  max={20000}
                  className="form-control"
                />
              </div>
            </div>

            <div className="row">
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controleEditpkg">
                    Category
                  </label>
                  <input
                    value={category}
                    readOnly={true}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controleEditpkg">
                    Start Date
                  </label>
                  <input
                    value={startDate}
                    onChange={(e) => {
                      setStartDate(e.target.value);
                    }}
                    type="date"
                    pattern="yyyy-mm-dd"
                    className="form-control"
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col">
                <label htmlFor="" className="label-controleEditpkg">
                  No of Days
                </label>
                <input
                  value={days}
                  readOnly={true}
                  // onChange={(e) => {
                  //     setDays(e.target.value);
                  // }}
                  type="number"
                  min={1}
                  max={10}
                  className="form-control"
                />
              </div>
              <div className="col">
                <label htmlFor="" className="label-controleEditpkg">
                  Transport Mode
                </label>

                <input
                  value={transportMode}
                  readOnly={true}
                  onChange={(e) => {
                    setTransportMode(e.target.value);
                  }}
                  type="text"
                  className="form-control"
                />
              </div>
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-controleEditpkg">
                Upload Image
              </label>
              <input
                onChange={(e) => {
                  setImageFile(e.target.files[0]);
                }}
                type="file"
                className="form-control"
              />
            </div>
            <table class="table">
              <tbody>
                <tr>
                  <td>
                    <button
                      onClick={EditDest}
                      type="button"
                      class="btn btn-primary"
                      disabled={show}
                    >
                      Save
                    </button>
                  </td>
                  <td>
                    <button
                      type="button"
                      class="btn btn-primary"
                      disabled={show}
                    >
                      Reset
                    </button>
                  </td>
                  {
                    <td>
                      <button
                        onClick={getdetails}
                        type="button"
                        class="btn btn-primary"
                        disabled={!show}
                      >
                        Get original Details
                      </button>
                    </td>
                  }
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
};

export default EditPackage;
