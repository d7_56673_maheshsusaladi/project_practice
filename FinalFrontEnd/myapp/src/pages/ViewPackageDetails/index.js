// ---------------- icons import------------------//
import iconBreakfast from "../../assets/icons/breakfast.png";
import iconLunch from "../../assets/icons/lunch.png";
import iconDinner from "../../assets/icons/dinner.png";
import icon1 from "../../assets/iconforNote.png";
import iconsightseeing from "../../assets/icons/sightseeing.png";
import iconfood from "../../assets/icons/food.png";
import iconlodging from "../../assets/icons/lodging.png";

// ------------------------------------------//
import "./index.css";
import { useLocation } from "react-router";
import axios from "axios";
import { useEffect, useState } from "react";
import { Accordion } from "react-bootstrap";
import { useNavigate } from "react-router";
const ViewPackageDetails = () => {
  const { state } = useLocation();
  const navigate = useNavigate();
  
  const { pkg } = state;
  const [schedule, setSchedule] = useState([]);
  let imageUrl = `http://localhost:8080/home/image/download/package/${pkg.packageId}`;
  const days = schedule.length;
  const getSchedule = () => {
    axios
      .get(`http://localhost:8080/home/package/schedule/${pkg.packageId}`)
      .then((response) => {
        const result = response.data;
        console.log(result.data);
        setSchedule(result.data);
      });
  };
  useEffect(() => {
    getSchedule();
  }, []);
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 divShape shadow">
            <img
              src={imageUrl}
              style={{ width: "100%", height: "500px" }}
              alt="none"
            ></img>
            <div className="divShape" style={{ backgroundColor: "whitesmoke" }}>
              <h4>Tour Details</h4>
            </div>
            <div>
              {schedule.map((s) => {
                return (
                  <div>
                    <div>
                      <Accordion defaultActiveKey={["0"]} alwaysOpen>
                        <Accordion.Item eventKey={s.dayNo}>
                          <Accordion.Header>
                            <h4 style={{ fontSize: "medium" }}>
                              Day {s.dayNo} {s.agenda}
                            </h4>
                          </Accordion.Header>
                          <Accordion.Body>
                            <div>{s.description}</div>
                            <div className="row">
                              {s.breakfast === 1 && (
                                <div className="col-md-1">
                                  <img
                                    style={{
                                      width: "30px",
                                      height: "30px",
                                    }}
                                    src={iconBreakfast}
                                    alt="none"
                                  />
                                </div>
                              )}
                              {s.lunch === 1 && (
                                <div className="col-md-1">
                                  <img
                                    style={{
                                      width: "30px",
                                      height: "30px",
                                    }}
                                    src={iconLunch}
                                    alt="none"
                                  />
                                </div>
                              )}
                              {s.dinner === 1 && (
                                <div className="col-md-1">
                                  <img
                                    style={{
                                      width: "30px",
                                      height: "30px",
                                    }}
                                    src={iconDinner}
                                    alt="none"
                                  />
                                </div>
                              )}
                            </div>
                            <div className="col-md-6"></div>
                          </Accordion.Body>
                        </Accordion.Item>
                      </Accordion>
                    </div>
                  </div>
                );
              })}
            </div>
            <div
              className="col divShape"
              style={{ backgroundColor: "whitesmoke" }}
            >
              <h4>Notes:</h4>
              <ul className="sample">
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  On tour Guests are requested to carry 2 photo copy (xerox) of
                  any Photo ID proof (Except Pan Card) i.e. Passport / Driving
                  Licence / Voter's ID / Adhar Card along with them. Also submit
                  a copy of the same at the time of booking.
                </p>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  Children / Students are requested to carry 2 photo copy
                  (xerox) of school or College Photo Identity card along with
                  them. Also submit a copy of the same at the time of booking.
                </p>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  For Infants carry 2 passport size photographs.
                </p>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  NRI guests are requested to carry their Passport & VISA copy.
                </p>
              </ul>
              <h5>Special Notes:</h5>
              <ul>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  Services like Tour Leader + extra toppings + Local cultural
                  performances will be applicable only if the tour group size is
                  7 or more than 7 pax.
                </p>
              </ul>
              <h5>Our Speciality:</h5>
              <ul>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  Caring tour manager through out the tour.
                </p>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  Insurance Cost is included in the Tour Price.
                </p>
              </ul>
              <h5>Road Transport:</h5>
              <ul>
                <p>
                  <img className="imgicon" src={icon1} alt="none"></img>
                  All Transfers and Sightseeing by A/C coach
                </p>
              </ul>
            </div>
          </div>
          <div
            className="col divShape shadow"
            style={{ height: "50%", marginLeft: "10px" }}
          >
            <p className="paragraph">
              <h5>Package Name:</h5> {pkg.packageName}
            </p>
            <p className="paragraph">
              <h5>Start Date:</h5> {pkg.startDate}
            </p>
            <p className="paragraph">
              <h5>Price per Person: </h5>
              {pkg.pricePerPerson}
            </p>
            <p className="paragraph">
              {" "}
              <h5>Total days:</h5> {pkg.days}
            </p>
            <p className="paragraph">
              <h5>Travelling mode:</h5> {pkg.transportMode}
            </p>
            <br></br>
            <div className="row">
              <div className="col-md-7">
                <h4>Facilities Provided</h4>
              </div>
              <div className="col-md-5">
                <img className="imgicon" src={iconsightseeing} alt="none"></img>
                <img className="imgicon" src={iconfood} alt="none"></img>
                <img className="imgicon" src={iconlodging} alt="none"></img>
                <img className="imgicon" src={iconDinner} alt="none"></img>
               
              </div>
              
            </div>
            <br></br>
            <div className="col-md-5">
                <button className="btn-primary btn-lg" onClick={()=>{
                  navigate("/book", { state: { id: pkg.packageId } });
                }}>BOOK </button>
              </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewPackageDetails;
