import { useState } from 'react'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import img from "../../images/OIP.jpg"

const SecretQuestion = () => {
  const [email, setEmail] = useState('')
  const [secretQue, setSecretQuestion] = useState('')
  const [secretAns, setSecretAnswer] = useState('')
  const [status, setStatus] = useState('')
  const no = 1
  const [disable, setDisable] = useState(false)
  const [newPassword, setNewPassword] = useState('')
  const [confirmNewPassword, setConfirmNewPassword] = useState('')

  const navigate = useNavigate()
  const secretquestion = () => {
    if (email.length === 0) {
      toast.warning('please enter your email')
    } else if (secretQue.length === 0) {
      toast.warning('please enter your Secret Question')
    } else if (secretAns.length === 0) {
      toast.warning('please enter your Secret Answer')
    } else {
      const body = {
        email,
        secretQue,
        secretAns,
      }

      // url to make secretquestion api call
      const url = `${URL}/user/forgotpassword`

      // make api call using axios
      axios.post(url, body).then((response) => {
        // get the server result
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          setStatus(no)
          console.log(status)

          setDisable(true)

        } else {
          toast.error('Invalid Secret Question or Secret Answer')
        }
      })
    }
  }
  const forgotpassword = () => {
    if (newPassword.length === 0) {
      toast.warning('please enter your New Password')
    } else if (confirmNewPassword.length === 0) {
      toast.warning('please enter Confirm New Password')
    }

    else {
      const body = {
        email,
        password: newPassword

      }
      const url = `${URL}/user/changepassword`
      axios.post(url, body).then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          toast.success('Password changed')
          navigate('/signin')
        } else {
          toast.error('FAIL!')
        }
      })
    }
  }
  return (
    <div>

      <section className="wrapper" style={{ marginTop: '20px' }}>
        <div className="container" >
          <div className="col-sm-8 offset-sm-2 col-lg-6  text-center">
            <div className="row rounded bg-white shadow p-4">
              <div className="logo">
                <img src={img} alt="logo" style={{ height: '150px', width: '350px', marginLeft: '25px' }} />
              </div>
              <h3 className="text-dark fw-bolder fs-4 mb-2">SECRET QUESTION</h3>
              <div className="form-floating mb-3">
                <div className="form-floating mb-3">

                  <input type="text" class="form-control" placeholder="EMAIL" onChange={(e) => { setEmail(e.target.value) }} />
                  <label for="floatingInput">Email</label>

                </div>

                <div className="form-floating mb-3">
                  <select id="inputState" class="form-select" onChange={(e) => { setSecretQuestion(e.target.value) }}>
                    <option selected>SECRET QUESTION</option>
                    <option>PET NAME</option>
                    <option>FAVOURITE ACTOR</option>
                    <option>SCHOOL NAME</option>
                  </select>
                </div>


                <div className="form-floating mb-3">

                  <input type="text" class="form-control" placeholder="ANSWER" onChange={(e) => { setSecretAnswer(e.target.value) }} />
                  <label for="floatingInput">ANSWER</label>

                </div>


                <div className="mb-3">

                  <br></br>
                  <button disabled={disable} onClick={secretquestion} className="btn btn-primary">
                    Submit
                  </button>
                </div>
              </div>
              {(status == 1 &&
              <div>
                <div className="row">
                  <div className="form-floating mb-3">

                    <input type="text" class="form-control" placeholder=" NEW PASSWORD" onChange={(e) => { setNewPassword(e.target.value) }} />
                    <label for="floatingInput">NEW PASSWORD</label>

                  </div>
                  <div className="form-floating mb-3">

                    <input type="text" class="form-control" placeholder=" CONFIRM PASSWORD" onChange={(e) => { setConfirmNewPassword(e.target.value) }} />
                    <label for="floatingInput">CONFIRM PASSWORD</label>

                  </div>
                </div>
                <br></br>
                <div className="row">
                  <div className="col"> <button onClick={forgotpassword} className="btn btn-primary btn-lg">
                    CHANGE PASSWORD
                  </button></div>
                </div>
              </div>
            )}
            </div>


           

          </div>
        </div>
      </section>
    </div>





  )
}

export default SecretQuestion