import Accordion from 'react-bootstrap/Accordion'
import '../AboutUs/about.css'
const AboutUs = () => {
  const mystyle = {
    fontFamily: 'Slabo 27px'

  };

  return (
    <div>
      <div></div>
      <br>
      </br>
      <div>
        <Accordion defaultActiveKey={['0']} alwaysOpen>
          <Accordion.Item eventKey="0">
            <Accordion.Header><h3>Find the best travel packages at Sunbeam Tours and Travels</h3></Accordion.Header>
            <Accordion.Body><div style={mystyle}>
              Booking a travel package when it comes to travelling to new parts of the country or the world is a practice that has slowly gained a lot of popularity. Today, whenever it is about planning a holiday trip, many people have a preferred travel portal in India that is best for their specific needs. Owing to the faith bestowed in our travel services by our patrons, Sunbeam Tours and Travels has established its niche and is counted among the top 10 travel agencies in Pune.

              We at Sunbeam Tours and Travels understand that nowadays, travelling has become much more than just visiting a new destination. That is why each of our vacation packages offers you the respite that you anticipate from a holiday. As a well-informed traveller, it is only right to expect more from your travel company in India - we strive to ensure the same for our customers. It is no longer about only conveyance and accommodation. For those who enjoy travelling, the best travel packages are those which can offer them holistic holiday experiences. That is exactly what you get when you opt for the best travel company in Mumbai – Sunbeam Tours and Travels.

              Counted among the top 10 travel agencies in Mumbai, Sunbeam Tours and Travels has all the travel services under one roof. In our constant endeavour to be the best travel company in India, everything that we do is based on creating and setting new benchmarks. With extensive travel know-how, end-to-end travel planning and a wide assortment of travel packages, we are counted among the best travel agents in India that ensure the best holiday experiences.
            </div></Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header><h3>Bringing you the best travel packages online</h3></Accordion.Header>
            <Accordion.Body><div div style={mystyle}>
              Making a niche for ourselves as a top online travel company in India,
              Sunbeam Tours and Travels excels in offering the best in domestic and international tour packages.
              Our product offerings include guided group tours, speciality tours, exclusive customised holiday packages, corporate MICE travel, and inbound travel.
              Behind our repute of being the best travel company in India is the vision to provide exclusivity in our tailor-made tours and packages that take you to a huge number of destinations in India and all around the world.
              Our travel company in India offers speciality tour packages such as Women’s Special, Honeymoon Tours, Seniors’ Packages, Singles Tour Packages, and Weekend Tours, which has helped us differentiate our offerings from other travel companies in Mumbai in both the domestic and international sectors.
              We offer a wide array of more than 2500 dream vacation options as well as a guest base of more than 5 lakh satisfied guests in a period of 8 years, which has played a major role in us becoming one of the top travel companies in Mumbai.
            </div></Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </div>
      <br></br>
      <br></br>
      
    </div>
  )
}

export default AboutUs
