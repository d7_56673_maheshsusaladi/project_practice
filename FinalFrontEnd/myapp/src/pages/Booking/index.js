import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import { useLocation } from "react-router";
import { useNavigate } from "react-router";
import "./style.css";
import busImg from "../../assets/icons/bus.png";
import planeImg from "../../assets/icons/plane.png";
import trainImg from "../../assets/icons/train.png";
const Booking = () => {
  const { state } = useLocation();
  const { id } = state;
  const navigate = useNavigate();
  const [counter, setCounter] = useState(0);
  const [personCount, setPersonCount] = useState("");
  const [persons, setPersons] = useState();
  const [booking, setBooking] = useState([]);
  const [enable, setEnable] = useState(false);
  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [idProof, setIdProof] = useState("");
  const userId = sessionStorage["userId"];
  const email = sessionStorage["email"];
  const mobileNo = sessionStorage["mobileNo"];
  const firstName = sessionStorage["firstName"];
  const lastName = sessionStorage["lastName"];
  const bookingDate = new Date().toJSON();
  const idProofLink = "id";
  const bookingPdfLink = "booking.pdf";
  const [pkg, setPkg] = useState("");

  sessionStorage.setItem("persons", persons);

  const arr = [];
  for (let index = 1; index <= personCount; index++) {
    arr.push(index);
  }

  // Role is user -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const book = () => {
    if (personCount.length == 0) {
      toast.warning("Enter No. of Persons");
    } else {
      const body = {
        personCount,
        persons,
        userId,
        packageId: pkg.packageId,
        bookingDate,
        idProofLink,
        bookingPdfLink,
      };

      // const url = `${URL}/user/booking/add`;
      // authAxios.post(url, body).then((response) => {
      //   const result = response.data;
      //   if (result["status"] == "success") {
      //     console.log(result.data);
      //     setBooking(result.data);
      //     setEnable(true);
      //   }
      // });
    }
    console.log(booking);
  };

  const addPerson = () => {
    if (counter == personCount) {
      setShow(true);
    } else if (name.length === 0) {
      toast.warning("Please enter name");
    } else if (age.length === 0) {
      toast.warning("Please enter age");
    } else if (idProof.length === 0) {
      toast.warning("Please select idproof");
    } else {
      const body = {
        bookingId: booking.bookingId,
        name,
        age,
        idProof,
      };

      const url = `${URL}/user/person/add`;
      authAxios.post(url, body).then((response) => {
        const result = response.data;
        if (result["status"] == "success") {
          toast.success(`${name} added`);

          setCounter(counter + 1);
        } else {
          toast.error("not added");
        }
      });
    }
  };

  const styles = {
    container: {
      marginBottom: "1%",
      borderRadius: "15px 15px 15px 15px",
      border: "solid",
      borderColor: "lightgrey",
      backgroundColor: "snow",
    },
  };

  useEffect(() => {
    const url = `http://localhost:8080/home/getpackage/${id}`;
    axios.get(url).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        console.log(result.data);
        setPkg(result.data);
      }
    });
  }, []);
  const gotopayment = () => {
    const pricePerPerson = pkg.pricePerPerson;
    const packageId = pkg.packageId;
    const bookingId = booking.bookingId;
    const packageName = pkg.packageName;
    const startDate = pkg.startDate;
    const days = pkg.days;
    const body = {
      pricePerPerson,
      packageId,
      bookingId,
      packageName,
      startDate,
      days,
    };
    navigate("/payment", { state: { body: body } });
  };
  return (
    <div>
      <h1 style={{ textAlign: "center", textStyle: "bold" }}>BOOKING FORM</h1>
      <div className="container shadow" style={styles.container}>
        <div style={{ padding: "20px" }}>
          <div className="row" style={{ marginTop: "30px" }}>
            <form className="row g-3">
              <div class="col-md-6">
                <label class="form-label">First Name</label>
                <input
                  type="text"
                  class="form-control"
                  value={firstName}
                  readOnly={true}
                />
              </div>
              <div class="col-md-6">
                <label class="form-label">Last Name</label>
                <input
                  type="text"
                  class="form-control"
                  value={lastName}
                  readOnly={true}
                />
              </div>
            </form>
          </div>
          <div className="row">
            <form className="row g-3">
              <div class="col-md-6">
                <label class="form-label">Email</label>
                <input
                  type="email"
                  class="form-control"
                  value={email}
                  readOnly={true}
                />
              </div>
              <div class="col-md-6">
                <label class="form-label">Phone Number</label>
                <input
                  type="number"
                  class="form-control"
                  value={mobileNo}
                  readOnly={true}
                />
              </div>
            </form>
          </div>
          <div className="row">
            <form action="" className="row g-3">
              <div class="col-6">
                <label class="form-label">PACKAGE NAME</label>
                <input
                  type="text"
                  class="form-control"
                  id="inputAddress"
                  value={pkg.packageName}
                  readOnly={true}
                />
              </div>
              <div class="col-6">
                <label class="form-label">PRICE/PERSON</label>
                <input
                  type="text"
                  class="form-control"
                  value={pkg.pricePerPerson}
                  readOnly={true}
                />
              </div>
            </form>

            <div className="row">
              <form action="" className="row g-3">
                <div class="col-4">
                  <label class="form-label">CATEGORY</label>
                  <input
                    type="text"
                    class="form-control"
                    value={pkg.category}
                    readOnly={true}
                  />
                </div>
                <div class="col-4">
                  <label for="inputCity" class="form-label">
                    START DATE
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    value={pkg.startDate}
                    readOnly={true}
                  />
                </div>
                <div class="col-4">
                  <label for="inputCity" class="form-label">
                    MODE OF TRANSPORT
                  </label>
                  <div>
                    {pkg.transportMode == "BUS" && (
                      <img className="transporticon" src={busImg} />
                    )}
                  </div>
                  <div>
                    {pkg.transportMode == "TRAIN" && (
                      <img className="transporticon" src={trainImg} />
                    )}
                  </div>
                  <div>
                    {pkg.transportMode == "FLIGHT" && (
                      <img className="transporticon" src={planeImg} />
                    )}
                  </div>
                </div>
              </form>
            </div>
            <div className="row">
              <form action="" className="row g-3">
                {/* <div className="col-4">
                  <label class="form-label">NO OF PERSONS</label>
                  <input
                    type="number"
                    class="form-control"
                    onChange={(e) => {
                      setPersonCount(e.target.value);
                    }}
                  />
                </div> */}
                {/* <div className="col-4 row g-4">
                  <button
                    disabled={enable}
                    type="button"
                    class="btn btn-primary "
                    style={{ paddingTop: "10px" }}
                    onClick={book}
                  >
                    ADD DETAILS
                  </button>
                </div> */}
              </form>
            </div>
            <br />
          </div>
        </div>
        <div className="row g-4">
          <div className="col-md-8">
            <div>
              <table className="table">
                <thead>
                  <th>FULL NAME</th>
                  <th>AGE</th>
                  <th>ID PROOF</th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <input
                        type="text"
                        className="form-control"
                        onChange={(e) => {
                          setName(e.target.value);
                        }}
                      ></input>
                    </td>
                    <td>
                      <input
                        type="number"
                        min={"10"}
                        max={"100"}
                        className="form-control"
                        onChange={(e) => {
                          if (e.target.value > 100) {
                            toast.warning("enter valid age");
                          }
                          setAge(e.target.value);
                        }}
                      ></input>
                    </td>
                    <td>
                      <select
                        className="form-select"
                        id="inputGroupSelect01"
                        onChange={(e) => {
                          setIdProof(e.target.value);
                        }}
                      >
                        <option value="">Choose....</option>
                        <option value="Adhar Card">Adhar Card</option>
                        <option value="PAN card">PAN Card</option>
                        <option value="Driving License">Driving License</option>
                      </select>
                    </td>
                    <td>
                      <button className="btn btn-primary" onClick={(e)=>{
                        setPersons(e.target.value)
                      }}>
                        Add
                      </button>
                    </td>
                    <td>
                      <button className="btn btn-danger">
                        Delete
                      </button>
                    </td>
                  </tr>
                </tbody>
                {/* <tbody>
                    {arr.map(() => {
                      return (
                        <tr>
                          <td>
                            <input
                              type="text"
                              className="form-control"
                              onChange={(e) => {
                                setName(e.target.value);
                              }}
                            ></input>
                          </td>
                          <td>
                            <input
                              type="number"
                              min={"10"}
                              max={"100"}
                              className="form-control"
                              onChange={(e) => {
                                if (e.target.value > 100) {
                                  toast.warning("enter valid age");
                                }
                                setAge(e.target.value);
                              }}
                            ></input>
                          </td>
                          <td>
                            <select
                              className="form-select"
                              id="inputGroupSelect01"
                              onChange={(e) => {
                                setIdProof(e.target.value);
                              }}
                            >
                              <option value="">Choose....</option>
                              <option value="Adhar Card">Adhar Card</option>
                              <option value="PAN card">PAN Card</option>
                              <option value="Driving License">
                                Driving License
                              </option>
                            </select>
                          </td>
                          <td>
                            <button
                              disabled={show}
                              className="btn btn-primary"
                              onClick={addPerson}
                            >
                              Add
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody> */}
                <br></br>
                {counter == personCount && (
                  <button
                    className="btn btn-primary btn-lg"
                    onClick={gotopayment}
                  >
                    Book
                  </button>
                )}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Booking;
