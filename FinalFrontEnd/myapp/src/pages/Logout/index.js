import { useEffect } from 'react';
import Accordion from 'react-bootstrap/Accordion'
import { useNavigate } from "react-router";
//import 'bootstrap/dist/css/bootstrap.min.css';
const Logout = () => {
  const navigate = useNavigate();
  sessionStorage.removeItem("loginStatus")
  sessionStorage.removeItem("firstName")
  sessionStorage.removeItem("lastName")
  sessionStorage.removeItem("userId")
  sessionStorage.removeItem("Authorization")
  sessionStorage.removeItem("email")
  sessionStorage.removeItem("mobileNo")
  sessionStorage.removeItem("pricePerPerson")
  sessionStorage.removeItem("packageId")
  useEffect(()=>{
    navigate('/home')
  },[])
 
  return(
  <div></div>
  )
     
  }
  
  export default Logout
  