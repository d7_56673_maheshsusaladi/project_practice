import axios from "axios";
import "./index.css";
import React, { useState, useEffect } from "react";
import { URL } from "../../config";
import userprofileicon from "../../assets/icons/userprofileicon.png";
const Profile = () => {
  const currentUserId = sessionStorage["userId"];
  console.log(currentUserId);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [secretQue, setSecretQue] = useState("");
  const [secretAns, setSecretAns] = useState("");
  const [data, setData] = useState([]);

  // Role is user -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const profile = () => {
    authAxios.get(`${URL}/user/profile/${currentUserId}`).then((response) => {
      const result = response.data;
      console.log(result);
      console.log(result.data);
      console.log(result.data.firstName);

      setData(response.data);
      setFirstName(result.data.userInfo.firstName);
      setLastName(result.data.userInfo.lastName);
      setEmail(result.data.userInfo.email);
      setAge(result.data.userInfo.age);
      setGender(result.data.userInfo.gender);
      setMobileNo(result.data.userInfo.mobileNo);
      setSecretQue(result.data.userInfo.secretQue);
      setSecretAns(result.data.userInfo.secretAns);
    });
  };

  useEffect(() => {
    profile();
  }, []);

  return (
    <div id="bottomContainerUser">
      <div style={{ padding: "10px" }} className="row">
        <div className="col"></div>
        <div className="col">
          <div
            className="form-control transferBoxuser"
            style={{ padding: "10px" }}
          >
            <div className="col">
              <center>
                <img
                  style={{ width: "30%", height: "30%" }}
                  src={userprofileicon}
                  alt="none"
                ></img>{" "}
              </center>
            </div>
            <h1 className="update">{firstName + " " + lastName}</h1>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Email
              </label>
              <input
                type="text"
                className="form-control"
                value={email}
                readOnly={true}
              />
            </div>
            <div className="row">
              <div className="col">
                <label htmlFor="" className="label-control-ud">
                  Age
                </label>
                <input
                  className="form-control"
                  readOnly="true"
                  value={age}
                ></input>
              </div>
              <div className="col">
                <label htmlFor="" className="label-control-ud">
                  Gender
                </label>
                <input
                  type="text"
                  className="form-control"
                  value={gender}
                  readOnly={true}
                />
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Mobile No
              </label>
              <input
                className="form-control"
                value={mobileNo}
                readOnly={true}
              ></input>
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Secret Question
              </label>
              <input
                type="text"
                className="form-control"
                value={secretQue}
                readOnly={true}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Secrete Answer
              </label>
              <input
                type="text"
                className="form-control"
                value={secretAns}
                readOnly={true}
              />
            </div>
            <div className="fw-normal text-muted mb-4">
              <a
                href="/updateprofile"
                className="text-primary fw-bold text-decoration-none"
              >
                <u>Go to update Profile page</u>
              </a>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
};

export default Profile;
