//import "bootstrap/dist/css/bootstrap.min.css";
// import destinations from '../../components/destinations'
import "./index.css";
import DestinationRow from "../../components/DestinationRow";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import axios from "axios";
const Destination = () => {
  const [destinations, setDestinations] = useState([]);
  const navigate = useNavigate();

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  useEffect(() => {
    authAxios.get("http://localhost:8080/admin/destinations").then((response) => {
      const result = response.data;
      setDestinations(result.data);
    });
  }, []);

  const AddNewDestination = () => {
    navigate("/admin/addnewdest");
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <br></br>
          <div>
            <button
              onClick={AddNewDestination}
              id="btnGroupDrop1"
              type="button"
              className="btn btn-primary float-right"
              aria-expanded="false"
            >
              {" "}
              Add New Destination
            </button>
            <br></br>
          </div>
          <br></br>
          <div class="shadow p-3 mb-5 bg-body rounded">
            <table className="table table-image">
              <thead>
                <tr>
                  <th scope="col">Destination Image</th>
                  <th scope="col">Destination Name</th>
                  <th scope="col">Action</th>
                  <th scope="col">Action</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {destinations.map((item) => {
                  return (
                    <DestinationRow
                      key={item.destinationId}
                      destination={item}
                    ></DestinationRow>
                  );
                })}
              </tbody>
            </table>
          </div>
          <br></br>
          <br></br>
        </div>
      </div>
    </div>
  );
};
export default Destination;
