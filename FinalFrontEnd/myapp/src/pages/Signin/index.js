import { toast } from "react-toastify";
import axios from "axios";
import { useNavigate } from "react-router";
import { URL } from "../../config";
import img from "../../images/OIP.jpg";
import { useState } from "react";

const Signin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  function parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );
    return JSON.parse(jsonPayload);
  }

  const signinUser = () => {
    if (email.length === 0) {
      toast.warning("please enter email");
    } else if (password.length === 0) {
      toast.warning("please enter password");
    } else {
      const body = {
        email,
        password,
      };

      // Post request for authenticate user
      const url = `${URL}/authenticate`;
      axios.post(url, body).then((response) => {
        const result = response.data;
        console.log(result);
        let role = parseJwt(result)["authorities"];
        sessionStorage.setItem("Authorization", "Bearer " + result);
        if (role === "admin") {
          sessionStorage.setItem("Authorization", "Bearer " + result);
          navigate("/destinations");
        } else {
          // Role is user -> hence call the signin method
          const authAxios = axios.create({
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Methods": "*",
              authorization: sessionStorage.getItem("Authorization"),
              Accept: "application/json",
              "Content-Type": "application/json",
            },
          });

          authAxios
            .post("http://localhost:8080/user/signin", body)
            .then((response) => {
              const result = response.data;
              if (result["status"] == "success") {
                toast.success("Welcome to the application");
                const { userId, firstName, lastName, email, mobileNo } = result.data;
                // persist the logged in user's information for future use
                sessionStorage["userId"] = userId;
                sessionStorage["firstName"] = firstName;
                sessionStorage["lastName"] = lastName;
                sessionStorage["loginStatus"] = 1;
                sessionStorage["email"] = email
                sessionStorage["mobileNo"] = mobileNo
                // navigate to home component
                navigate("/home");
              } else {
                toast.error("Invalid username or password");
              }
            });
        }
      });
    }
  };

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col"></div>
          <div className="col-md-5 text-center">
            <div className="logo">
              <img
                src={img}
                alt="logo"
                style={{
                  height: "150px",
                  width: "350px",
                }}
              />
            </div>
            <form className="rounded bg-white shadow p-5">
              <h3 className="text-dark fw-bolder fs-4 mb-2">SIGNIN</h3>
              <div className="fw-normal text-muted mb-4">
                No Account?
                <a
                  href="/signup"
                  className="text-primary fw-bold text-decoration-none"
                >
                  Signup here
                </a>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="email"
                  class="form-control"
                  id="floatingInput"
                  placeholder="name@example.com"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
                <label for="floatingInput">Email address</label>
              </div>
              <div class="form-floating">
                <input
                  type="password"
                  class="form-control"
                  id="floatingPassword"
                  placeholder="Password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
                <label for="floatingPassword">Password</label>
              </div>
              <div className="mt-2 text-end">
                <a
                  href="/secretquestion"
                  className="text-primary fw-bold text-decoration none"
                >
                  Forgot Password?
                </a>
              </div>

             
            </form>
            <button
              type="submit"
              className="btn btn-primary submit_btn w-100 my-4"
              onClick={signinUser}
            >
              Signin
            </button>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </div>
  );
};
export default Signin;
