//import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import PackageRow from "../../components/PackageRow";
import { useEffect, useState } from "react";
import axios from "axios";
import { useLocation, useNavigate } from "react-router";
const Package = () => {
  const [packages, setPackages] = useState([]);
  const { state } = useLocation();
  const { id } = state;
  const navigate = useNavigate();

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const getAllPackages = () => {
    const url = `http://localhost:8080/admin/getall/${id}`;
    authAxios.get(url).then((response) => {
      const result = response.data;
      console.log(url);
      if (result.status === "success") {
        setPackages(result.data);
      } else {
        console.log("Error in fetching the data..");
      }
    });
  };

  useEffect(() => {
    getAllPackages();
  }, []);

  const AddNewpackage = () => {
    navigate("/addnewpackage", { state: { id: id } });
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div>
            <br></br>
            <button
              onClick={AddNewpackage}
              id="btnGroupDrop1"
              type="button"
              className="btn btn-primary float-right"
              aria-expanded="false"
            >
              {" "}
              Add New Package
            </button>
            <br></br>
          </div>
          <br></br>
          <div class="shadow p-3 mb-5 bg-body rounded">
            <table className="table table-image">
              <thead>
                <tr>
                  <th scope="col">Package Image</th>
                  <th scope="col">Package Name</th>
                  <th scope="col">Action</th>
                  <th scope="col">Action</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {packages.map((item) => {
                  return <PackageRow packages={item}></PackageRow>;
                })}
              </tbody>
            </table>
          </div>
          <br></br>
          <br></br>
        </div>
      </div>
    </div>
  );
};
export default Package;
