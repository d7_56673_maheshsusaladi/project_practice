//import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import { useNavigate } from "react-router";
import { useState } from "react";
import { toast } from "react-toastify";
import { useLocation } from "react-router";
import axios from "axios";
const UpdateDestination = () => {
  const { state } = useLocation();
  const { id } = state;

  const fillDetails = () => {
    const url = `http://localhost:8080/admin/destination/${id}`;
    authAxios.get(url).then((response) => {
      const result = response.data;
      console.log(result.data.countryName);
      setCountryName(result.data.countryName);
      setStateName(result.data.stateName);
      setCountryInfo(result.data.countryInfo);
      setStateInfo(result.data.stateInfo);
    });
  };

  const [countryName, setCountryName] = useState("");
  const [countryInfo, setCountryInfo] = useState("");
  const [stateName, setStateName] = useState("");
  const [stateInfo, setStateInfo] = useState("");
  const [imageFile, setImageFile] = useState("");
  const destinationId = id;
  const navigate = useNavigate();

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const UpdateDest = () => {
    if (countryName.length === 0) {
      toast.info("Please enter Country Name");
    } else if (countryInfo.length === 0) {
      toast.info("Please enter CountryInfo");
    } else if (stateInfo.length === 0) {
      toast.info("Please enter StateInfo");
    } else {
      const body = {
        destinationId,
        countryName,
        countryInfo,
        stateName,
        stateInfo,
      };
      authAxios
        .post("http://localhost:8080/admin/edit_destination", body)
        .then((response) => {
          const result = response.data;
          uploadFile();
          navigate("/destinations");
          toast.success("Destination " + result.data);
        });

      // File upload code
      const uploadFile = () => {
        const formdata = new FormData();
        formdata.append("datafile", imageFile);

        const config = {
          headers: {
            "content-type": "multipart/form-data",
          },
        };

        let url = `http://localhost:8080/admin/image/upload/${id}`;
        console.log(imageFile);
        console.log(url);
        authAxios.post(url, formdata, config).then((response) => {
          const result = response.data;
          if (result.status === "success") {
            toast.success("File uploaded successfully....");
            navigate("/destinations");
          }
        });
      };
    }
  };
  return (
    <div className="imagedest">
      <div style={{ padding: "10px" }} className="row">
        <div className="col"></div>
        <div className="col">
          <div className="form-control transferbox" style={{ padding: "10px" }}>
            <h1 className="update">Update Destination</h1>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Country Name
              </label>
              <input
                value={countryName}
                type="text"
                className="form-control"
                required={true}
                readOnly={true}
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Country Info
              </label>
              <textarea
                value={countryInfo}
                onChange={(e) => {
                  setCountryInfo(e.target.value);
                }}
                className="form-control"
              ></textarea>
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                State Name
              </label>
              <input
                value={stateName}
                type="text"
                className="form-control"
                required={true}
                readOnly={true}
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                State Info
              </label>
              <textarea
                value={stateInfo}
                onChange={(e) => {
                  setStateInfo(e.target.value);
                }}
                className="form-control"
              ></textarea>
            </div>
            <br></br>
            <div className="mb-3">
              <label htmlFor="" className="label-control-ud">
                Upload Image
              </label>
              <input
                type="file"
                className="form-control"
                required={true}
                onChange={(e) => {
                  setImageFile(e.target.files[0]);
                }}
              />
              <br></br>
              <div className="row">
                <div className="col">
                  <button
                    onClick={UpdateDest}
                    id="btnGroupDrop1"
                    type="button"
                    className="btn btn-primary float-right"
                    aria-expanded="false"
                  >
                    {" "}
                    Save
                  </button>
                </div>
                <div className="col">
                  <button
                    onClick={fillDetails}
                    id="btnGroupDrop1"
                    type="button"
                    className="btn btn-primary float-right"
                    aria-expanded="false"
                  >
                    {" "}
                    Get Details
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
};

export default UpdateDestination;
