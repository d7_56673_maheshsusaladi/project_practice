import axios from "axios";
import { useLocation } from "react-router";
import { useEffect, useState } from "react";
import { Accordion } from "react-bootstrap";
import "./index.css";
const PackageDetails = () => {
  const { state } = useLocation();
  const [pkg, setPkg] = useState([]);
  const [schedule, setSchedule] = useState([]);
  const { id } = state;
  const url = `http://localhost:8080/admin/getpackage/${id}`;
  let imageUrl = `http://localhost:8080/home/image/download/package/${id}`;

  // Role is admin ->
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const getDetails = () => {
    authAxios.get(url).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        console.log(result.data);
        setPkg(result.data);
      }
    });
  };

  const getSchedule = () => {
    authAxios
      .get(`http://localhost:8080/admin/package/schedule/${id}`)
      .then((response) => {
        const result = response.data;
        console.log(result.data);
        setSchedule(result.data);
      });
  };

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <div className="imagepack" style={{ padding: "10px" }}>
      <div className="row">
        {/* <div className="col"></div> */}
        <div className="col-md-6">
          <div className="form-control">
            <h2 className="styling">{pkg.packageName}</h2>
            <img
              style={{ width: "100%", height: "10%" }}
              src={imageUrl}
              alt={pkg.packageName}
            ></img>
            <div className="row">
              <div className="col">
                <div className="mb-6">
                  <label htmlFor="" className="label-controlepkg">
                    Package Name
                  </label>
                  <input
                    value={pkg.packageName}
                    readOnly={true}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controlepkg">
                    Price/Person
                  </label>
                  <input
                    value={pkg.pricePerPerson}
                    readOnly={true}
                    type="number"
                    className="form-control"
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controlepkg">
                    Category
                  </label>
                  <input
                    value={pkg.category}
                    readOnly={true}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controlepkg">
                    Start Date
                  </label>
                  <input
                    value={pkg.startDate}
                    readOnly={true}
                    type="date"
                    pattern="yyyy-mm-dd"
                    className="form-control"
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controlepkg">
                    No of Days
                  </label>
                  <input
                    value={pkg.days}
                    readOnly={true}
                    type="number"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col">
                <div className="mb-3">
                  <label htmlFor="" className="label-controlepkg">
                    Transport Mode
                  </label>

                  <input
                    value={pkg.transportMode}
                    readOnly={true}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>
            </div>
            <div>
              <button onClick={getSchedule} className="btn btn-primary">
                Get Schedule
              </button>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="form-control">
            <h5 className="stylingh5">Package Schedule</h5>
            <table>
              {schedule.map((s) => {
                return (
                  <div>
                    <div>
                      <Accordion defaultActiveKey={["0"]} alwaysOpen>
                        <Accordion.Item eventKey={s.dayNo}>
                          <Accordion.Header>
                            <h3 style={{ fontSize: "medium" }}>{s.agenda}</h3>
                          </Accordion.Header>
                          <Accordion.Body>
                            <div>{s.description}</div>
                          </Accordion.Body>
                        </Accordion.Item>
                      </Accordion>
                    </div>
                  </div>
                );
              })}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PackageDetails;

{
  /* <div className="form-control">
        <table>
          <th></th>
          <tbody>
            <tr>
              Package Id:
              <td>{pkg.packageId}</td>
            </tr>
            <tr>
              Package Name:
              <td>{pkg.packageName}</td>
            </tr>
            <tr>
              Price per person:
              <td>{pkg.pricePerPerson}</td>
            </tr>
            <tr>
              Category:
              <td>{pkg.category}</td>
            </tr>
            <tr>
              Start Date:
              <td>{pkg.startDate}</td>
            </tr>
            <tr>
              No of Days:
              <td>{pkg.days}</td>
            </tr>
            <tr>
              Transport Mode:
              <td>{pkg.transportMode}</td>
            </tr>
            <tr>
              <td>Package Image</td>
            </tr>
            <tr>
              <td>
                <img src={pkg.pkgImage} alt={pkg.packageName}></img>
              </td>
            </tr>
            <br></br>
            <tr>
              <td>
                <button onClick={getSchedule} className="btn btn-primary">
                  Get package Schedule
                </button>
              </td>
            </tr>
            <tr>
              <br></br>
            </tr>
          </tbody>
        </table>
        <table>
          {schedule.map((s) => {
            return (
              <div>
                <div>
                  <Accordion defaultActiveKey={["0"]} alwaysOpen>
                    <Accordion.Item eventKey={s.dayNo}>
                      <Accordion.Header>
                        <h3 style={{ fontSize: "large" }}>{s.agenda}</h3>
                      </Accordion.Header>
                      <Accordion.Body>
                        <div>{s.description}</div>
                        <button
                          style={{ alignSelf: "flex-end" }}
                          className="btn btn-primary"
                        >
                          Edit Day
                        </button>
                      </Accordion.Body>
                    </Accordion.Item>
                  </Accordion>
                </div>
              </div>
            );
          })}
        </table> */
}
