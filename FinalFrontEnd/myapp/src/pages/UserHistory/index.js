import { toast } from "react-toastify";
import axios from "axios";
import { useNavigate } from "react-router";
import { URL } from "../../config";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

// Role is user -> hence call the post booking method
const authAxios = axios.create({
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "*",
    authorization: sessionStorage.getItem("Authorization"),
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

const History = (props) => {
  const { history } = props;
  const currentDate = new Date().toJSON();

  return (
    <tr>
      <td className="w-10">{history[0]}</td>
      <td>{history[1]}</td>
      <td>{history[2]}</td>
      <td>{history[3]}</td>
    </tr>
  );
};
const UserHistory = () => {
  const currentUserId = sessionStorage["userId"];

  useEffect(() => {
    document.title = "User History";
    getAllHistory();
  }, []);

  const url = `${URL}/user/history/${currentUserId}`;
  const getAllHistory = () => {
    authAxios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] == "success") {
        setHistory(result.data.userInfo);
      } else {
        toast.error("error");
      }
    });
  };
  const [history, setHistory] = useState([]);
  console.log("location are " + history.data);
  console.log("location " + history);

  return (
    <div>
      <h1>Booking History</h1>
      <div className="container">
        <div className="row" style={{ margin: "10px" }}>
          <div className="col"></div>
          <div className="col-8">
            <table
              className="table table-hover shadow "
              style={{ backgroundColor: "floralwhite" }}
            >
              <thead>
                <tr>
                  <th scope="col">Package Name</th>
                  <th scope="col">Total Price</th>
                  <th scope="col">Booking Date {"(yyyy-MM-dd)"}</th>
                  <th scope="col">Person Count</th>
                </tr>
              </thead>
              <tbody>
                {history.map((item) => {
                  return <History history={item} />;
                })}
              </tbody>
              <div className="mb-3">
                <div>
                  <br></br>
                  <Link to={{ pathname: "/profile" }}>Go to Profile page</Link>
                </div>
              </div>
            </table>
          </div>
          <div className="col"></div>
        </div>
      </div>
      <div></div>
    </div>
  );
};

export default UserHistory;
