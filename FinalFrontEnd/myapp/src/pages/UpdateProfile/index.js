import { useState } from "react";
import "./index.css";
import { toast } from "react-toastify";
import axios from "axios";
import { useNavigate } from "react-router";
import { URL } from "../../config";

const UpdateProfile = () => {
  const userId = sessionStorage["userId"];
  console.log(userId);

  const [age, setAge] = useState("");
  const [mobileNo, setMobileNumber] = useState("");

  const navigate = useNavigate();

  // Role is user -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const updateprofile = () => {
    if (age.length == 0) {
      toast.warning("please enter your New Age");
    } else if (mobileNo.length == 0) {
      toast.warning("please enter your New Mobile Number");
    } else {
      const body = {
        userId,
        age,
        mobileNo,
      };
      // url to make updateprofile api call
      const url = `${URL}/user/updateprofile`;
      // make api call using axios
      authAxios.post(url, body).then((response) => {
        // get the server result
        const result = response.data;
        console.log(result);
        if (result["status"] == "success") {
          toast.success("Profile Updated");

          // get the data sent by server
          const { email, age, mobileNo } = result["data"];

          // persist the logged in user's information for future use
          sessionStorage["email"] = email;
          sessionStorage["age"] = age;
          sessionStorage["mobilenumber"] = mobileNo;

          // navigate to profile component
          navigate("/profile");
        } else {
          toast.error("Data not get updated");
        }
      });
    }
  };

  return (
    <div>
      <br></br>
      <section className="wrapper" style={{ marginTop: "20px" }}>
        <div className="container">
          <div className="col-sm-8 offset-sm-2 col-lg-6 offset-lg-3  text-center">
            <br></br>
            <form className="rounded bg-white shadow p-4">
              <img
                style={{ width: "80px" }}
                src="https://img.icons8.com/external-wanicon-lineal-color-wanicon/64/000000/external-user-online-security-wanicon-lineal-color-wanicon.png"
              />

              <h3 className="text-dark fw-bolder fs-4 mb-2">UPDATE PROFILE</h3>
              <div class="form-floating mb-3">
                <input
                  type="text"
                  class="form-control"
                  placeholder="abc"
                  onChange={(e) => {
                    setAge(e.target.value);
                  }}
                />
                <label for="floatingInput">Age</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="text"
                  class="form-control"
                  placeholder="xyz"
                  onChange={(e) => {
                    setMobileNumber(e.target.value);
                  }}
                />
                <label for="floatingInput">Mobile Number</label>
              </div>
            </form>
            <div class="d-grid gap-2" style={{ marginTop: "10px" }}>
              <button
                className="btn btn-primary btn-lg w_100"
                onClick={updateprofile}
              >
                UPDATE{" "}
              </button>
            </div>
          </div>
        </div>
        <div></div>
      </section>
    </div>
  );
};

export default UpdateProfile;
