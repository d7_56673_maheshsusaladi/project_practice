import { toast } from "react-toastify";
import axios from "axios";
import { useNavigate } from "react-router";
import { URL } from "../../config";
import { useState } from "react";
import img from "../../images/OIP.jpg";
const Signup = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [secretQue, setSecretQue] = useState("");
  const [secretAns, setSecretAns] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [gender, setGender] = useState("");
  const [age, setAge] = useState("");
  const role = "user";

  const navigate = useNavigate();

  const signupUser = () => {
    if (firstName.length === 0) {
      toast.warning("Please enter first name");
    } else if (lastName.length === 0) {
      toast.warning("Please enter last name");
    } else if (email.length === 0) {
      toast.warning("Please enter email");
    } else if (password.length === 0) {
      toast.warning("Please enter password");
    } else if (confirmPassword.length === 0) {
      toast.warning("Please confirm your password");
    } else if (password !== confirmPassword) {
      toast.warning("Password does not match");
    } else if (mobileNo.length === 0) {
      toast.warning("Please enter Mobile NO");
    } else {
      const body = {
        firstName,
        lastName,
        email,
        password,
        mobileNo,
        secretQue,
        secretAns,
        age,
        gender,
        role,
      };

      const url = `${URL}/home/signup`;
      axios.post(url, body).then((response) => {
        const result = response.data;
        console.log(result);
        if (result["status"] === "success") {
          toast.success("Successfully signed up new user");
          navigate("/signin");
        } else {
          toast.error(result["error"]);
        }
      });
    }
  };

  return (
    <div>
      <section className="wrapper" style={{ marginTop: "20px" }}>
        <div className="container">
          <div className="row">
            <div className="col"></div>
            <div className="col-md-5">
              <div className="logo">
                <img
                  src={img}
                  alt="logo"
                  style={{
                    height: "150px",
                    width: "350px",
                    marginLeft: "90px",
                    marginRight: "25px",
                  }}
                />
              </div>
              <form className="rounded bg-white shadow p-4">
                <h3 className="text-dark fw-bolder fs-4 mb-2">
                  Careate Account
                </h3>
                <div className="fw-normal text-muted mb-3">
                  Already have account?
                  <a
                    href="/signin"
                    className="text-primary fw-bold text-decoration-none"
                  >
                    Signin here
                  </a>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="abc"
                    onChange={(e) => {
                      setFirstName(e.target.value);
                    }}
                  />
                  <label for="floatingInput">First Name</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="xyz"
                    onChange={(e) => {
                      setLastName(e.target.value);
                    }}
                  />
                  <label for="floatingInput">Last Name</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="email"
                    class="form-control"
                    placeholder="name@example.com"
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                  />
                  <label for="floatingInput">Email Address</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="password"
                    class="form-control"
                    placeholder="Password"
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                  <label for="floatingPassword">Password</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="password"
                    class="form-control"
                    placeholder="ConfirmPassword"
                    onChange={(e) => {
                      setConfirmPassword(e.target.value);
                    }}
                  />
                  <label>Confirm Password</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="number"
                    class="form-control"
                    placeholder="Contact"
                    onChange={(e) => {
                      setMobileNo(e.target.value);
                    }}
                  />
                  <label>Contact Number</label>
                </div>
                <div class="form-floating mb-3">
                  <div className="row">
                    <div className="col">
                      <select
                        id="inputState"
                        class="form-select"
                        onChange={(e) => {
                          setGender(e.target.value);
                        }}
                      >
                        <option selected>GENDER</option>
                        <option>MALE</option>
                        <option>FEMALE</option>
                        <option>OTHER</option>
                      </select>
                    </div>
                    <div className="col">
                      <input
                        type="number"
                        class="form-control"
                        placeholder="AGE"
                        onChange={(e) => {
                          setAge(e.target.value);
                        }}
                      />
                      <label></label>
                    </div>
                  </div>
                </div>
                <div class="form-floating mb-3">
                  <div className="row">
                    <div className="col">
                      <select
                        id="inputState"
                        class="form-select"
                        onChange={(e) => {
                          setSecretQue(e.target.value);
                        }}
                      >
                        <option selected>SECRET QUESTION</option>
                        <option>PET NAME</option>
                        <option>FAVOURITE ACTOR</option>
                        <option>SCHOOL NAME</option>
                      </select>
                    </div>
                    <div className="col">
                      <input
                        type="password"
                        class="form-control"
                        placeholder="ANSWER"
                        onChange={(e) => {
                          setSecretAns(e.target.value);
                        }}
                      />
                      <label></label>
                    </div>
                  </div>
                </div>
              </form>
              <div class="d-grid gap-2" style={{ marginTop: "10px" }}>
                <button
                  className="btn btn-primary btn-lg w_100"
                  onClick={signupUser}
                >
                  SIGN UP{" "}
                </button>
              </div>
            </div>
            <div className="col"></div>
          </div>
        </div>
        <div></div>
      </section>
    </div>
  );
};

export default Signup;
