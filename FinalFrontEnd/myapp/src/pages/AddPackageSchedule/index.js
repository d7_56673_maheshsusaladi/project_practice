import { useLocation } from "react-router";
import DaysScheduleAdd from "../../components/DaywiseScheduleAdd";

const AddPackageSchedule = () => {
  const { state } = useLocation();
  const { id } = state;
  return <DaysScheduleAdd id={id}></DaysScheduleAdd>;
};
export default AddPackageSchedule;
