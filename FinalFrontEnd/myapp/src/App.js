import "./App.css";
import img1 from "../src/assets/icons8-instagram-100.png";
import img2 from "../src/assets/icons8-facebook-circled-100.png";
import img3 from "../src/assets/icons8-twitter-circled-100.png";
// ------------ Pages imports ---------------------
import Signin from "./pages/Signin";
import Signup from "./pages/Signup";
import Home from "./pages/Home";
import SecretQuestion from "./pages/SecretQuestion";
import UpdateProfile from "./pages/UpdateProfile";
import Profile from "./pages/Profile";
import AboutUs from "./pages/AboutUs";
import Logout from "./pages/Logout";
import UserHistory from "./pages/UserHistory";
import UpdateDestination from "./pages/UpdateDestination";
import AddNewDestination from "./pages/AddNewDestination";
import AddNewPackage from "./pages/AddNewPackage";
import Destination from "./pages/Destination";
import PackageDetails from "./pages/PackageDetails";
import Package from "./pages/Packages";
import Schedule from "./pages/Schedule";
import UpdateSchedule from "./pages/UpdateSchedule";
import EditPackage from "./pages/EditPackage";
import AddPackageSchedule from "./pages/AddPackageSchedule";
import DisplayPackages from "./pages/DisplayPackages";
import ViewPackageDetails from "./pages/ViewPackageDetails";
// ------------------------------------------------------------
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
//import 'react-toastify/dist/ReactToastify.css'

import Booking from "./pages/Booking/index";
import Payment from "./pages/Payment";
//import 'bootstrap/dist/css/bootstrap.min.css';
//import { Container } from "reactstrap";

function App() {
  const mystyle = {
    color: "darkorange",
    fontFamily: "Dancing Script",
  };

  return (
    <div>
      {/* <div className="container"> */}

      <BrowserRouter>
        <Routes>
          <Route path="/signin" element={<Signin />} />
          <Route path="/payment" element={<Payment />} />
          <Route path="/book" element={<Booking />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/secretquestion" element={<SecretQuestion />} />
          <Route path="/home" element={<Home />} />
          <Route path="/updateprofile" element={<UpdateProfile />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/destinations" element={<Destination />}>
            {" "}
          </Route>
          <Route path="/packages" element={<Package />}>
            {" "}
          </Route>
          <Route path="/packageDetails" element={<PackageDetails />}>
            {" "}
          </Route>
          <Route path="/updatedest" element={<UpdateDestination />}>
            {" "}
          </Route>
          <Route path="/admin/addnewdest" element={<AddNewDestination />}>
            {" "}
          </Route>
          <Route path="/addnewpackage" element={<AddNewPackage />}>
            {" "}
          </Route>
          <Route path="/updateSchedule" element={<UpdateSchedule />}>
            {" "}
          </Route>
          <Route path="/schedule" element={<Schedule />}>
            {" "}
          </Route>
          <Route path="/history" element={<UserHistory />}>
            {" "}
          </Route>
          <Route path="/editPackage" element={<EditPackage />}></Route>
          <Route path="/addPackageSchedule" element={<AddPackageSchedule />}>
            {" "}
          </Route>
          <Route path="/allPackages" element={<DisplayPackages />}></Route>
          <Route path="/viewDetails" element={<ViewPackageDetails />}></Route>
        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
      <div>
        <div class="shadow p-1 mb-3 bg-body rounded">
          <footer class="site-footer">
            <div class="container">
              <div class="row">
                <div class="col-sm-12 col-md-6">
                  <br></br>
                  <h6>About</h6>
                  <p class="text-justify">
                    Booking a travel package when it comes to travelling to new
                    parts of the country or the world is a practice that has
                    slowly gained a lot of popularity. Today, whenever it is
                    about planning a holiday trip, many people have a preferred
                    travel portal in India that is best for their specific
                    needs.
                  </p>
                </div>

                <div class="col-6 col-md-3">
                  <br></br>
                  <h6>Quick Links</h6>
                  <ul class="footer-links">
                    <li>
                      <a href="Home">Home</a>
                    </li>
                    <li>
                      <a href="Signin">SignIn</a>
                    </li>
                  </ul>
                </div>

                <div class="col-6 col-md-3">
                  <br></br>
                  <h6>Contact Us</h6>
                  <ul class="footer-links">
                    <h6>Mobile : +91 9988978765</h6>
                    <h6>Email : punetours@gmail.com</h6>
                  </ul>
                </div>
              </div>
              <hr />
              <div>
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                      <p>
                        Copyright &copy; 2021 All Rights Reserved By PuneTours
                      </p>
                    </div>
                    <div class="col-md-4 col-sm-9 col-12">
                      <ul class="list-group list-group-horizontal">
                        <li class="list-group-item">
                          <img class="img-responsive2" src={img2} alt="none" />
                        </li>
                        <br></br>
                        <li class="list-group-item">
                          <img class="img-responsive2" src={img1} alt="none" />
                        </li>
                        <br></br>
                        <li class="list-group-item">
                          <img class="img-responsive2" src={img3} alt="none" />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default App;
