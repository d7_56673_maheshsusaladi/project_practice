import { useNavigate } from "react-router";
import "./styles.css";
import {
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Button,
  Col,
  Row,
} from "reactstrap";
const Image = (props) => {
  const { destination } = props;
  let imageUrl = `http://localhost:8080/home/image/download/${destination.destinationId}`;
  const navigate = useNavigate();

  const showPackages = () => {
    navigate("/allPackages", { state: { id: destination.destinationId } });
  };

  return (
    <Card
      className="shadow p-2 mb-3 bg-white rounded cardPopup"
      style={{ width: "320px", margin: "30px" }}
    >
      <CardImg
        className="card-img-top "
        style={{ width: "300px", height: "200px"}}
        src={imageUrl}
        alt={destination.stateName}
      />
      <CardBody>
        <CardTitle style={{fontWeight:"bold", fontSize:"medium"}}>{destination.stateName}</CardTitle>
        {/* <CardSubtitle>{destination.stateName}</CardSubtitle> */}
        <CardText>{destination.stateInfo}</CardText>
        <div className="row g-2">
          <div className="col">
            <button onClick={showPackages} className="btn btn-warning">
              Show Packages
            </button>
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

export default Image;
{
  /* <div class="card-footer">
<small class="text-muted">Last updated 3 mins ago</small>
</div> */
}
