
import { useState } from "react";
const DaysSchedule = (props) => {
  const { data } = props;
  const day = data.dayNo;
  console.log(day)

  const [agenda, setAgenda] = useState("");

  const editDay = (day) => {
    return <h2>Me called</h2>;
  };

  return (
    <div>
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Day No</th>
            <th scope="col">Agenda</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {day.map((i) => {
            return (
              <tr>
                <th scope="row">{i.dayno}</th>
                <td>{i.agenda}</td>
                <td>{i.description}</td>
                <td>
                  <button onClick={editDay} className="btn btn-primary">
                    Edit
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default DaysSchedule;
