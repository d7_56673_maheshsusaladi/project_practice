const schedules = {
  pkgid: 1,
  day: [
    {
      dayno: 1,
      agenda: "Visit to panhala",
      description: "This is day 1",
    },
    {
      dayno: 2,
      agenda: "Visit to Rankala",
      description: "This is day 2",
    },
    {
      dayno: 3,
      agenda: "Visit to Mahalaxmi Temple",
      description: "This is day 3",
    },
    {
      dayno: 4,
      agenda: "Visit to Jotiba",
      description: "This is day 4",
    },
  ],
};
// {
//   pkgid: 2,
//   day: [
//     {
//       dayno: 1,
//       agenda: "package2 day 1",
//     },
//     {
//       dayno: 2,
//       agenda: "package2 day 2",
//     },
//     {
//       dayno: 3,
//       agenda: "package2 day 3",
//     },
//   ],
// },
// {
//   pkgid: 3,
//   day: [
//     {
//       dayno: 1,
//       agenda: "package3 day 1",
//     },
//     {
//       dayno: 2,
//       agenda: "package3 day 2",
//     },
//     {
//       dayno: 3,
//       agenda: "package3 day 3",
//     },
//     {
//       dayno: 4,
//       agenda: "package4 day 4",
//     },
//     {
//       dayno: 5,
//       agenda: "package4 day 5",
//     },
//   ],
// },

export default schedules;
