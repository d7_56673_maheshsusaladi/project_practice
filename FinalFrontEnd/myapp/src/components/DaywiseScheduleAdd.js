import axios from "axios";
import { useState } from "react";
import { toast } from "react-toastify";

const AddDaywiseSchedule = (props) => {
  const { num } = props;
  const dayNo = num;
  console.log(dayNo);
  const packageId = sessionStorage.getItem("packageId");
  console.log(packageId);
  const [agenda, setAgenda] = useState("");
  const [description, setDescription] = useState("");
  const [breakfast, setBreakfast] = useState("");
  const [dinner, setDinner] = useState("");
  const [lunch, setLunch] = useState("");
  let [count, setCount] = useState(0);
  const [enable, setEnable] = useState(false);

  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const AddSchedule = () => {
    if (agenda.length === 0) {
      toast.warning("Please enter agenda");
    } else if (description.length === 0) {
      toast.warning("Please add description");
    } else {
      const body = {
        agenda,
        description,
        dayNo,
        packageId,
        breakfast,
        dinner,
        lunch,
      };
      console.log(body);
      setEnable(true);
      authAxios
        .post("http://localhost:8080/admin/add_schedule", body)
        .then((response) => {
          const result = response.data;
          if (result.status === "success") {
            toast.success(`${agenda} added succesfully`);
            setEnable(true);
            setCount(num);
            console.log(count);
          } else {
            toast.error(`${agenda} failed to addd`);
          }
        });
    }
  };
  return (
    <tr>
      <td className="col-mb-3">
        <label>{num}</label>
      </td>
      <td className="col-md-4">
        <input
          type="text"
          className="form-control"
          onChange={(e) => {
            setAgenda(e.target.value);
          }}
        ></input>
      </td>
      <td className="col-md-6">
        <textarea
          style={{ fontFamily: "cursive" }}
          type="text"
          className="form-control"
          rows="1"
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        ></textarea>
      </td>
      <td className="md-col">
        <button
          disabled={enable}
          className="btn btn-primary"
          onClick={AddSchedule}
        >
          Add
        </button>
      </td>
      <td>
        <input
          type="checkbox"
          onClick={(e) => {
            setBreakfast("1");
          }}
        ></input>
      </td>
      <td>
        <input
          type="checkbox"
          onClick={(e) => {
            setLunch("1");
          }}
        ></input>
      </td>
      <td>
        <input
          type="checkbox"
          onClick={(e) => {
            setDinner("1");
          }}
        ></input>
      </td>
    </tr>
  );
};
export default AddDaywiseSchedule;
