import "react-toastify/dist/ReactToastify.css";
//import "bootstrap/dist/css/bootstrap.min.css";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import axios from "axios";
const DestinationRow = (props) => {
  const { destination } = props;
  const navigate = useNavigate();
  // Role is admin ->
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  let imageUrl = `http://localhost:8080/home/image/download/${destination.destinationId}`;
  const getPackages = () => {
    navigate("/packages", { state: { id: destination.destinationId } });
  };
  const updateDestination = () => {
    navigate("/updatedest", { state: { id: destination.destinationId } });
  };

  const deleteDestination = () => {
    const url = `http://localhost:8080/admin/delete_destination/${destination.destinationId}`;
    console.log(url);
    authAxios.delete(url).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        toast.success("Destination deleted successfully");
        window.location.reload();
      } else {
        toast.error("Failed to delete destination");
      }
    });
  };

  return (
    <tr>
      <td className="w-10   ">
        <img
          style={{ width: "250px", height: "200px" }}
          src={imageUrl}
          className="shadow p-2 mb-5 bg-white rounded"
          alt={destination.stateName}
        />
      </td>
      <td className="name">{destination.stateName}</td>
      <td>
        <button
          onClick={updateDestination}
          type="button"
          class="btn btn-primary"
        >
          Edit
        </button>
      </td>
      <td>
        <button
          onClick={deleteDestination}
          type="button"
          class="btn btn-danger"
        >
          Delete
        </button>
      </td>
      <td>
        <button onClick={getPackages} type="button" class="btn btn-primary">
          Manage Packages
        </button>
      </td>
    </tr>
  );
};
export default DestinationRow;
