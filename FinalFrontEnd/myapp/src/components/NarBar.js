import { Navbar, Nav, NavDropdown } from "react-bootstrap";
const NavBar = () => {
  let email = sessionStorage.getItem("email");
  console.log(email)
  return (
    <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
      {/* <Container> */}
      <Navbar.Brand href="Home">Home</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link href="AboutUs">AboutUs</Nav.Link>
          <NavDropdown title="Signin/Signup" id="collasible-nav-dropdown">
            <NavDropdown.Item href="Signin">Signin</NavDropdown.Item>
            <NavDropdown.Item href="Signup">Signup</NavDropdown.Item>
          </NavDropdown>
        </Nav>
        <Nav>
          {email && (
            <NavDropdown title={email} id="collasible-nav-dropdown">
              <NavDropdown.Item href="Profile">Profile</NavDropdown.Item>
              <NavDropdown.Item href="/history">History</NavDropdown.Item>
              <NavDropdown.Item href="Logout">Logout</NavDropdown.Item>
            </NavDropdown>
          )}
        </Nav>
      </Navbar.Collapse>
      {/* </Container> */}
    </Navbar>
  );
};
export default NavBar;
