//import "bootstrap/dist/css/bootstrap.min.css";
import { useNavigate } from "react-router";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useEffect, useState } from "react";
const PackageRow = (props) => {
  // Role is admin -> hence call the post booking method
  const authAxios = axios.create({
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      authorization: sessionStorage.getItem("Authorization"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const { packages } = props;
  const navigate = useNavigate();
  let imageUrl = `http://localhost:8080/home/image/download/package/${packages.packageId}`;

  // get package details
  const getPackageDetails = () => {
    navigate("/packageDetails", { state: { id: packages.packageId } });
  };

  // Edit package
  const EditPackage = () => {
    navigate("/editPackage", { state: { id: packages.packageId } });
  };

  const AddPackageSchedule = () => {
    navigate("/addPackageSchedule", { state: { id: packages.packageId } });
  };

  // Delete package
  const DeletePackage = () => {
    const url = `http://localhost:8080/admin/package/delete/${packages.packageId}`;
    console.log(url);
    authAxios.delete(url).then((response) => {
      const result = response.data;
      if (result.status === "success") {
        toast.success("Package deleted successfully");
        // navigate("/packages", { state: { id: packages.destinationId } });
        window.location.reload();
      } else {
        toast.error("Failed to delete package");
      }
    });
  };
  return (
    <tr>
      <td className="w-25">
        <img
          style={{ width: "250px", height: "200px" }}
          src={imageUrl}
          className="shadow p-3 mb-5 bg-white rounded"
          alt={packages.packageName}
        />
      </td>
      <td className="name">{packages.packageName}</td>
      <td>
        <button onClick={EditPackage} type="button" class="btn btn-primary">
          Edit
        </button>
      </td>
      <td>
        <button onClick={DeletePackage} type="button" class="btn btn-danger">
          Delete
        </button>
      </td>

      <td>
        <button
          onClick={getPackageDetails}
          type="button"
          class="btn btn-primary"
        >
          Show Details
        </button>
      </td>
    </tr>
  );
};
export default PackageRow;
