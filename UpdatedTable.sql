
create database project;

use project;

create table user(userId int primary key auto_increment,firstName VARCHAR(50),lastName VARCHAR(50),email VARCHAR(100),password VARCHAR(500),age int,gender VARCHAR(50),mobileNo VARCHAR(100),secretQue VARCHAR(200),secretAns VARCHAR(200),role VARCHAR(50));

insert into user values(0,'Saurabh','Kale','saurabh@test.com','1234',25,'Male','9874563210','which is your favorite movie','ZNMD','admin');

create table destination(destinationId int primary key auto_increment,stateName VARCHAR(100),stateInfo VARCHAR(500),countryName VARCHAR(100),countryInfo VARCHAR(500),destImage VARCHAR(500));

insert into destination values(0,'Maharashtra',' Maharashtra is divided into 6 divisions and 36 districts','India','India, officially the Republic of India is a country in South Asia.','https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
insert into destination values(0,'Goa','Goa is visited by large numbers of international and domestic tourists each year because of its white-sand beaches, active nightlife, places of worship, and World Heritage-listed architecture.','India','India, officially the Republic of India is a country in South Asia.','https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
insert into destination values(0,'Gujarat','Gujarats natural environment includes the Great Rann of Kutch and the hills of Saputara','India','India is a megadiverse country, a term employed for 17 countries which display high biological diversity ','https://image.kesari.in/upload/homepageImage/Akshardam_Gujarat_KesariTours.jpg?v=22');

create table package(packageId int primary key auto_increment,packageName VARCHAR(500),pricePerPerson decimal(9,2),category VARCHAR(200),startDate DATE,days int,transportMode varchar(50),destinationId int, FOREIGN KEY(destinationId) REFERENCES destination(destinationId),pkgImage VARCHAR(500));
//old
insert into package values(0,'Kolapur',500,'Family','2022-03-21',5,'Bus',1,'https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
insert into package values(0,'Sangli',400,'Family','2022-04-01',6,'Air',1,'https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
insert into package values(0,'Solapur',300,'Group','2022-04-08',4,'Train',1,'https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
insert into package values(0,'Satara',200,'Solo','2022-04-15',4,'Bus',1,'https://image.kesari.in//media/bucket//Goa/Goa.jpg?v=22');
//updated
insert into package values(0,'GOA WITH LAKSHADWEEP CRUISE',800,'Family','2022-03-21',5,'Cruise',2,'https://d3r8gwkgo0io6y.cloudfront.net/upload/W5/goa.jpg?v=22');
insert into package values(0,'GUJARAT RANN UTSAV',1000,'Family','2022-04-01',6,'Air',3,'https://d3r8gwkgo0io6y.cloudfront.net/upload/WR/Ran-Utsav-1-(1).jpg?v=22');
insert into package values(0,'NASHIK SAPUTARA',800,'Group','2022-04-08',4,'Train',1,'https://d3r8gwkgo0io6y.cloudfront.net/upload/assets/index/Mauritius-new.jpg');
insert into package values(0,'TARKARLI',900,'Solo','2022-04-15',4,'Bus',1,'https://d3r8gwkgo0io6y.cloudfront.net/upload/M7/scuba-diving-tarkarl-Malvan-(1).jpg?v=22');

//after adding date in booking table

create table booking(bookingId int primary key auto_increment,personCount int,totalPrice decimal(9,2),idProofLink VARCHAR(500),bookingPdfLink VARCHAR(500),userId int,packageId int,bookingDate DATE, FOREIGN KEY(userId) REFERENCES user(userId),FOREIGN KEY(packageId) REFERENCES package(packageId));

insert into booking values(0,2,1000,'abc.com','bookingpdf.com',1,1,"2020-03-31");
insert into booking values(0,3,1200,'pqr.com','bookingpdf.com',1,2,"2021-04-30");
insert into booking values(0,6,1800,'xyz.com','bookingpdf.com',1,3,"2022-05-15");
insert into booking values(0,1,200,'std.com','bookingpdf.com',1,4,"2021-06-10");

create table dayWiseSchedule(scheduleId int primary key auto_increment, agenda varchar(500), description varchar(2000), dayNo int,packageId int, foreign key(packageId) REFERENCES package(packageId));

insert into dayWiseSchedule values(0,"Historical Places","Shaniwar Wada Palace is considered as one of the most popular tourist places in Pune and also an eminent historical landmark.then visit to Dagdusheth Halwai Temple.then visit Vetal Tekdi is another famous spot in Pune, where you can see the entire city standing on the top of the Vetal hill.",1,1);
insert into dayWiseSchedule values(0," Kelkar Museum and Parvati Hill","Dr. Dinkar G Kelkar who made this in memory of his son, Raja. The museum has several sculptures from the 14th century, ",2,1);
insert into dayWiseSchedule values(0,"Arrive at Goa","Arrive at Goa – A perfect holiday destination. On arrival free time to relax and enjoy resort or the beach.",1,2);
insert into dayWiseSchedule values(0,"Goa","Today proceed to visit Shri Shanta Durga Temple dedicated to Goddess of peace, Mangeshi Temple. Later explore Tropical Spice Plantation which is untouched by polution and makes you feel a part of nature. Later visit Old Goa Church.",2,2);
insert into dayWiseSchedule values(0,"Cruise - Lakshadweep - On bard","Today visit India’s best kept secret, Lakshadweep - a paradise of sun-kissed beaches, translucent blue waters and luscious landscape. Visit Kadmat Island also known as Cardamom Island, famous for marine turtles and coral reefs. You can also check out thrilling water sports like kayaking, snorkeling or scuba diving at an additional cost.",3,2);
insert into dayWiseSchedule values(0,"At Sea","Begin the day with breakfast and experience the different fun and adventure activities we have onboard for you and your family. Enjoy some fun family time at the pool. Indulge in the gourmet cuisines and enjoy a cocktail at the Pool Bar. We have several activities and events planned for the evenings. Don’t forget to try your luck at the casino.",4,2);
insert into dayWiseSchedule values(0,"Arrive at Mumbai","Disembark the cruise at Mumbai port. Its time to say goodbye to your travel companions.",5,2);



