INSERT INTO users(id, name, password, mobileNo, email) VALUES
(1, 'admin','$2a$10$ZSGoV9rZOPJ0nthd/Y7l5O3PPKs7zgOY62uCXqKvpY0O/A6zACh6m',8275916161,'admin@test.com')
INSERT INTO users(id, name, password, mobileNo, email) VALUES
(2, 'nilesh','$2a$10$ZSGoV9rZOPJ0nthd/Y7l5O3PPKs7zgOY62uCXqKvpY0O/A6zACh6m',8275916161,'user@test.com'),


INSERT INTO roles(id, user_id, role) VALUES
(1, 1, 'ROLE_ADMIN'),
(2, 2, 'ROLE_USER'),
