CREATE TABLE users(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(30), password VARCHAR(100), mobileNo VARCHAR(15), email VARCHAR(40));

CREATE TABLE roles(id INT AUTO_INCREMENT PRIMARY KEY, user_id INT, role VARCHAR(30));
