package com.example.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.daos.BookingDao;
import com.example.daos.PackageDao;
import com.example.dto.BookingDto;
import com.example.dto.BookingDtoEntity;
import com.example.dto.PersonDetailsDto;
import com.example.dto.PersonDtoEntity;
import com.example.entities.Booking;

@Service
@Transactional
public class BookingSevice {

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	BookingDtoEntity converter;

	@Autowired
	private PersonDtoEntity converterPerson;

	@Autowired
	private PackageDao packageDao;

	// delete booking
	public Map<String, Object> deleteBooking(int id) {

		if (bookingDao.existsById(id)) {
			bookingDao.deleteById(id);
			return Collections.singletonMap("deleted..", id);
		} else {
			return Collections.singletonMap("failed to delete..", id);
		}

	}

	// Add new Booking
	public BookingDto addNewBooking(BookingDto dto) {
		Booking booking = converter.toBookingEntity(dto);
		Booking save = bookingDao.save(booking);
		return converter.toBookingDto(save);
	}

	// Total price calculation 
	public double getTotalPriceForBooking(int bookingId) {
		List<PersonDetailsDto> list = bookingDao.findById(bookingId).orElse(null).getPerson().stream()
				.map(p -> converterPerson.toPersonDetailsDto(p)).collect(Collectors.toList());

		// Method for calculating price per person from package
		Booking booking = bookingDao.findById(bookingId).orElse(null);
		int packageId = converter.toBookingDto(booking).getPackageId();
		double pricePerPerson = packageDao.findByPackageId(packageId).getPricePerPerson();
		double sum = 0;
		for (int i = 0; i < list.size(); i++) 
		{
			
			if (list.get(i).getAge() < 18) 
			{	
			    sum = sum + (pricePerPerson * 0.7);
			}
			else 
			{
				sum = sum + pricePerPerson;
			}
		}
		System.out.println("Your Total Price for this package is : "+sum);
		
		booking.setTotalPrice(sum);
		bookingDao.save(booking);
		return sum;
		
		
	}

	// get person details from booking Id
	public List<PersonDetailsDto> getPeopleFromBookingId(int bookingId) {
		List<PersonDetailsDto> list = bookingDao.findById(bookingId).orElse(null).getPerson().stream()
				.map(p -> converterPerson.toPersonDetailsDto(p)).collect(Collectors.toList());
        return list;
	}
	
}