package com.example.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.daos.PackageDao;
import com.example.dto.PackageDto;
import com.example.dto.PackageDtoEntity;
import com.example.dto.ScheduleDto;
import com.example.dto.ScheduleDtoToEntitiy;
import com.example.entities.Packagee;

@Service
@Transactional
public class PackageService {

	private static String DIRECTORY_PATH = "E:\\project_practice\\ProjectImages\\PackageImages";
//	private static String DIRECTORY_PATH = "F:\\CDAC Exam Push\\PROJECT\\PROJECT PRACTICE\\project_practice\\ProjectImages\\PackageImages\\";
	@Autowired
	private PackageDao packageDao;
	@Autowired
	private PackageDtoEntity converter;
	@Autowired
	private ScheduleDtoToEntitiy converterSchedule;

	// Get all packages
	public List<PackageDto> getAll() {
		return packageDao.findAll().stream().map(p -> converter.toPackageDto(p)).collect(Collectors.toList());
	}

	// find package by id
	public PackageDto findById(int id) {
		Packagee packagee = packageDao.findById(id).orElse(null);
		return converter.toPackageDto(packagee);
	}

	// Add new Package
	public PackageDto addNewPackage(PackageDto dto) {
		Packagee newPackage = converter.toPackageEntity(dto);
		Packagee save = packageDao.save(newPackage);
		return converter.toPackageDto(save);
	}

	// delete package by id
	public Map<String, Object> deletePackageByid(int id) {
		if (packageDao.existsById(id)) {
			packageDao.deleteById(id);
			return Collections.singletonMap("deleted", id);
		} else {
			return Collections.singletonMap("failed to delete...", id);
		}
	}

	// update package
	public String updatePackage(PackageDto dto) {
		Packagee updatePkg = packageDao.findById(dto.getPackageId()).orElse(null);
		if (updatePkg != null) {
			updatePkg.setPkgImage(dto.getPkgImage());
			updatePkg.setStartDate(dto.getStartDate());
			updatePkg.setPricePerPerson(dto.getPricePerPerson());
			packageDao.save(updatePkg);
			return "Package updated successfully";
		} else {
			return "No package found!!!!!";
		}
	}

	public List<PackageDto> getAllPackgesofCity(String cityName) {
		List<Packagee> packages = packageDao.findPackageeByPackageName(cityName);
		return packages.stream().map(p -> converter.toPackageDto(p)).collect(Collectors.toList());
	}

	public List<ScheduleDto> getPackageSchedule(int id) {
		List<ScheduleDto> listSchedule = packageDao.getById(id).getSchedule().stream()
				.map(s -> converterSchedule.toScheduleDto(s)).collect(Collectors.toList());

		return listSchedule;
	}

	// Upload image for package
	public Map<String, Object> uploadPackageImage(MultipartFile file, int id) throws IOException {
		File convertFile = new File(DIRECTORY_PATH + file.getOriginalFilename());
		convertFile.createNewFile();
		FileOutputStream out = new FileOutputStream(convertFile);
		out.write(file.getBytes());
		out.close();
		String filepath = DIRECTORY_PATH + file.getOriginalFilename();
		Packagee destToUpdate = packageDao.findByPackageId(id);
		destToUpdate.setPkgImage(filepath);
		Packagee save = packageDao.save(destToUpdate);
		return Collections.singletonMap("updated path:", save.getPkgImage());
	}

	// Download package image from file system
	public ByteArrayResource downloadPackageImage(int id) throws IOException {
		Packagee findById = packageDao.findById(id).orElse(null);
		System.out.println(findById.getPackageName());
		String filePath = packageDao.findById(id).orElse(null).getPkgImage();
		System.out.println(filePath);
		ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(filePath)));
		return inputStream;
	}
}
