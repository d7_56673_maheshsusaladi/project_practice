package com.example.service;

import java.util.Collections;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.daos.DayWiseScheduleDao;
import com.example.daos.PackageDao;
import com.example.dto.ScheduleDto;
import com.example.dto.ScheduleDtoToEntitiy;
import com.example.entities.DayWiseSchedule;
import com.example.entities.Packagee;

@Service
@Transactional
public class ScheduleService {

	@Autowired
	private DayWiseScheduleDao scheduleDao;
	
	@Autowired
	private PackageDao packageDao;
	
	@Autowired
	private ScheduleDtoToEntitiy converter;
	
	public Map<String,Object> addSchedule(ScheduleDto newSchedule)
	{
		DayWiseSchedule Entity = converter.dtoToEntity(newSchedule);
		 scheduleDao.save(Entity);
		
			 return Collections.singletonMap("inserted id", Entity.getScheduleId());
		 
	}
	
	public DayWiseSchedule udpate(ScheduleDto dto)
	{
		 DayWiseSchedule db=scheduleDao.findByScheduleId(dto.getScheduleId());
		 Packagee pkg =packageDao.findByPackageId(dto.getPackageId());
		 db.setAgenda(dto.getAgenda());
		 db.setDayNo(dto.getDayNo());
		 db.setDescription(dto.getDescription());
		 db.setPackagee(pkg);
		 
		   DayWiseSchedule result=scheduleDao.save(db);
		   return result;
		 
	}
	
}
