package com.example.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.daos.PersonDetailsDao;
import com.example.dto.PersonDetailsDto;
import com.example.dto.PersonDtoEntity;
import com.example.entities.PersonDetails;

@Service
@Transactional
public class PersonDetailsService {
	@Autowired
	private PersonDetailsDao dao;

	@Autowired
	private PersonDtoEntity converter;

	// Add person details for particular booking
	public  PersonDetails addPersonDetails(PersonDetailsDto dto) {
		PersonDetails personDetailsEntity = converter.toPersonDetailsEntity(dto);

		PersonDetails save = dao.save(personDetailsEntity);
		return save;
	}

//	// Add List of persons in booking
//	public Map<String, Object> addPersons(List<PersonDetailsDto> list) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		for (PersonDetailsDto person : list) {
//			PersonDetails personDetailsEntity = converter.toPersonDetailsEntity(person);
//			PersonDetails save = dao.save(personDetailsEntity);
//			map.put("inserted id", save.getId());
//		}
//		return map;
//	}
}
