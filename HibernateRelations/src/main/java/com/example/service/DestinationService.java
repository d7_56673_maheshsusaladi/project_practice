package com.example.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.daos.DestinationDao;
import com.example.dto.DestinationDto;
import com.example.dto.DestinationDtoEntity;
import com.example.dto.PackageDto;
import com.example.dto.PackageDtoEntity;
import com.example.entities.Destination;

@Transactional
@Service
public class DestinationService {

	private static final String DIRECTORY_PATH = "E:\\project_practice\\ProjectImages\\DestinationImages\\";
	
	@Autowired
	private DestinationDao destinationDao;

	@Autowired
	private PackageDtoEntity pkgdtoentity;

	@Autowired
	private DestinationDtoEntity converter;

	@Autowired
	private PackageDtoEntity converterpkg;

	// Get all packages for particular destination
	public List<PackageDto> getById(int id) {
		Destination destination = destinationDao.findById(id).orElse(null);
		return destination.getPackageList().stream().map(p -> pkgdtoentity.toPackageDto(p))
				.collect(Collectors.toList());

	}

	// Delete destination by Id
	public String deleteDestination(int id) {

		if (destinationDao.existsById(id)) {
			destinationDao.deleteById(id);
			return "Destination deleted....";
		} else {
			return "Destination deletion failed";
		}

	}

	// Add new Destination
	public DestinationDto addNewDestination(DestinationDto destDto) {
		System.out.println(destDto);
		Destination destination = converter.toDestinationEntity(destDto);
		destination = destinationDao.save(destination);
		return converter.toDestinationDto(destination);
	}

	// get All destinations for particular country
	public List<DestinationDto> getallDestinationsInCountry(String countryName) {
		List<Destination> destinations = destinationDao.findDestinationByCountryName(countryName);
		return destinations.stream().map(d -> converter.toDestinationDto(d)).collect(Collectors.toList());
	}

	// Update destination info
	public String updateDestination(DestinationDto destdto) {
		Destination toBeupdated = destinationDao.findByDestinationId(destdto.getDestinationId());
		toBeupdated.setStateInfo(destdto.getStateInfo());
		toBeupdated.setDestImage(destdto.getDestImage());
		destinationDao.save(toBeupdated);
		return "Updated successfully";
	}

	// show all destinations
	public List<DestinationDto> getAllDestinations() {
		return destinationDao.findAll().stream().map(d -> converter.toDestinationDto(d)).collect(Collectors.toList());
	}

	// get All packages in particular state
	public List<PackageDto> getPackagesInState(String stateName) {
		Destination destination = destinationDao.findDestinationByStateName(stateName);
		return destination.getPackageList().stream().map(p -> converterpkg.toPackageDto(p))
				.collect(Collectors.toList());
	}

	// get all packages in particular id
	public List<PackageDto> getAllPackages(int id) {
		Destination destination = destinationDao.findByDestinationId(id);
		return destination.getPackageList().stream().map(p -> converterpkg.toPackageDto(p))
				.collect(Collectors.toList());
	}

	// find destination by destination id
	public DestinationDto findByDestinationId(int id) {
		Destination destination = destinationDao.findByDestinationId(id);
		return converter.toDestinationDto(destination);
	}

	// Upload image for Destination
	public Map<String, Object> uploadImage(MultipartFile file, int id) throws IOException {
		File newFile = new File(DIRECTORY_PATH + file.getOriginalFilename());
		newFile.createNewFile();
		FileOutputStream out = new FileOutputStream(newFile);
		out.write(file.getBytes());
		out.close();
		String filepath = DIRECTORY_PATH + file.getOriginalFilename();
		Destination destToUpdate = destinationDao.findByDestinationId(id);
		destToUpdate.setDestImage(filepath);
		Destination save = destinationDao.save(destToUpdate);
		return Collections.singletonMap("updated path:", save.getDestImage());
	}

	// Download destination File from file system
	public ByteArrayResource downloadPhoto(int id) throws IOException {
		String filePath = destinationDao.getById(id).getDestImage();
		System.out.println(filePath);
		ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(filePath)));
		return inputStream;
	}

	// Add new Destination
	public DestinationDto addNewDestinationForUpload(DestinationDto destDto) {
		System.out.println(destDto);
		Destination destination = converter.toDestinationEntity(destDto);
		destination = destinationDao.save(destination);
		return converter.toDestinationDto(destination);
	}
}
