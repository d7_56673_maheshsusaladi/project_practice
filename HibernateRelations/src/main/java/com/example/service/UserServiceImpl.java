package com.example.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.daos.UserDao;
import com.example.dto.Credential;
import com.example.dto.QuestionCredential;
import com.example.dto.UserDto;
import com.example.dto.UserDtoEntityConverter;
import com.example.entities.User;

@Transactional
@Service
public class UserServiceImpl {

	@Autowired
	private UserDao userDao;
	@Autowired
	UserDtoEntityConverter converter;
	@Autowired
	private PasswordEncoder passwordEncoder;

	// Checking if email already exists
	public Object saveUser(User newUser) {

		User existingUser = userDao.findByEmail(newUser.getEmail());
		System.out.println(existingUser);
		if (existingUser != null)
			return "Email already Exists!";

		String rawPassword = newUser.getPassword();
		String encPassword = passwordEncoder.encode(rawPassword);

		newUser.setPassword(encPassword);
		userDao.save(newUser);
		UserDto newUserDto = converter.toUserDto(newUser);
		return newUserDto;
	}

	// User sign in
	public UserDto findByEmailAndPassword(Credential cred) {

		User dbUser = userDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		if (dbUser != null && passwordEncoder.matches(rawPassword, dbUser.getPassword())) {
			UserDto userDto = converter.toUserDto(dbUser);
			userDto.setPassword("**************");
			return userDto;
		}
		return null;

	}

	// forgot password
	public boolean forgotPassword(QuestionCredential question) {
		User user = userDao.findByEmail(question.getEmail());
		if (user.getSecretAns().equals(question.getSecretAns())) {
			return true;
		}
		return false;

	}

	// Change password
	public boolean changePassword(Credential cred) {
		User user = userDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		String encPassword = passwordEncoder.encode(rawPassword);
		user.setPassword(encPassword);
		User savedUser = userDao.save(user);
		if (savedUser != null)
			return true;
		return false;

	}

	// Update user age and password
	public UserDto updateProfile(User user) {
		User updateUser = userDao.findByUserId(user.getUserId());

		updateUser.setAge(user.getAge());
		updateUser.setMobileNo(user.getMobileNo());
		userDao.save(updateUser);
		UserDto updatedUser = converter.toUserDto(updateUser);
		return updatedUser;
	}

	//  User Profile
	public Map<String, Object> findUserInfoBYId(int id) {
		User user = userDao.findById(id).orElse(null);
		if (user != null) {
			UserDto dto1 = converter.toUserDto(user);
			return Collections.singletonMap("userInfo", dto1);
		}
		return null;
	}

	// Booking history of user
	public Map<String, Object> findUserHistoryById(int id) {
		if (userDao.existsById(id)) {
			List<Object> result = userDao.findUserHistoryById(id);
			return Collections.singletonMap("userInfo", result);
		}
		return null;
	}
}
