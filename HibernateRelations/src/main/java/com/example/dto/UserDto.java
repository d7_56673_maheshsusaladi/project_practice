package com.example.dto;

public class UserDto {

	private int userId;
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private int age;
	private String gender;
	private String mobileNo;
	private String secretQue;
	private String secretAns;
	
	public UserDto() {
		super();
	}

	public UserDto(int userId, String email, String password, String firstName, String lastName, int age, String gender,
			String mobileNo, String secretQue, String secretAns) {
		super();
		this.userId = userId;
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.mobileNo = mobileNo;
		this.secretQue = secretQue;
		this.secretAns = secretAns;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSecretQue() {
		return secretQue;
	}

	public void setSecretQue(String secretQue) {
		this.secretQue = secretQue;
	}

	public String getSecretAns() {
		return secretAns;
	}

	public void setSecretAns(String secretAns) {
		this.secretAns = secretAns;
	}

	@Override
	public String toString() {
		return "UserDto [userId=" + userId + ", email=" + email + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", age=" + age + ", gender=" + gender + ", mobileNo=" + mobileNo
				+ ", secretQue=" + secretQue + ", secretAns=" + secretAns + "]";
	}

	
}
