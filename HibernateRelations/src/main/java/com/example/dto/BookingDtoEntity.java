package com.example.dto;

import org.springframework.stereotype.Component;

import com.example.entities.Booking;
import com.example.entities.Packagee;
import com.example.entities.User;

@Component
public class BookingDtoEntity {

	public BookingDto toBookingDto(Booking entity) {
		if (entity != null) {
			BookingDto dto = new BookingDto();
			dto.setBookingId(entity.getBookingId());
			dto.setPersonCount(entity.getPersonCount());
			dto.setTotalPrice(entity.getTotalPrice());
			dto.setIdProofLink(entity.getIdProofLink());
			dto.setPackageId(entity.getPackagee().getPackageId());
			dto.setUserId(entity.getUser().getUserId());
			dto.setBookingDate(entity.getBookingDate());
			dto.setBookingPdfLink(entity.getBookingPdfLink());
			
			return dto;
		}
		return null;
	}

	public Booking toBookingEntity(BookingDto dto) {
		Booking entity = new Booking();
		entity.setBookingId(dto.getBookingId());
		entity.setIdProofLink(dto.getIdProofLink());
		entity.setPersonCount(dto.getPersonCount());
		entity.setTotalPrice(dto.getTotalPrice());
		User user = new User();
		user.setUserId(dto.getUserId());
		entity.setUser(user);
		Packagee newpackage = new Packagee();
		newpackage.setPackageId(dto.getPackageId());
		entity.setPackagee(newpackage);
		entity.setBookingDate(dto.getBookingDate());
		entity.setBookingPdfLink(dto.getBookingPdfLink());
		
		return entity;
	}
}
