package com.example.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.daos.PackageDao;
import com.example.entities.DayWiseSchedule;
import com.example.entities.Packagee;

@Component
public class ScheduleDtoToEntitiy {

	@Autowired
	private PackageDao packageDao;

	public DayWiseSchedule dtoToEntity(ScheduleDto dto) {
		Packagee pkg = packageDao.findByPackageId(dto.getPackageId());

		DayWiseSchedule newSchedule = new DayWiseSchedule();

		newSchedule.setScheduleId(dto.getScheduleId());
		newSchedule.setAgenda(dto.getAgenda());
		newSchedule.setDescription(dto.getDescription());
		newSchedule.setDayNo(dto.getDayNo());
		newSchedule.setPackagee(pkg);
		newSchedule.setBreakfast(dto.getBreakfast());
		newSchedule.setLunch(dto.getLunch());
		newSchedule.setDinner(dto.getDinner());
		return newSchedule;

	}

	public ScheduleDto toScheduleDto(DayWiseSchedule entity) {
		ScheduleDto dto = new ScheduleDto();
		dto.setScheduleId(entity.getScheduleId());
		dto.setAgenda(entity.getAgenda());
		dto.setDescription(entity.getDescription());
		dto.setDayNo(entity.getDayNo());
		dto.setPackageId(entity.getPackagee().getPackageId());
		dto.setBreakfast(entity.getBreakfast());
		dto.setLunch(entity.getLunch());
		dto.setDinner(entity.getDinner());
		return dto;
	}

}
