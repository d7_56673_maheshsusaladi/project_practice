package com.example.dto;

import org.springframework.stereotype.Component;

import com.example.entities.User;

@Component
public class UserDtoEntityConverter {


	
	public UserDto toUserDto(User user)
	{
		UserDto dto =new UserDto();

		dto.setUserId(user.getUserId());
		dto.setEmail(user.getEmail());
		dto.setPassword(user.getPassword());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setAge(user.getAge());
		dto.setGender(user.getGender());
		dto.setMobileNo(user.getMobileNo());
		dto.setSecretQue(user.getSecretQue());
		dto.setSecretAns(user.getSecretAns());
		
		return dto;
	}

	
	public User toUser(UserDto dto)
	{
		User newUser= new User();
		newUser.setUserId(dto.getUserId());
		newUser.setEmail(dto.getEmail());
		newUser.setPassword(dto.getPassword());
		newUser.setFirstName(dto.getFirstName());
		newUser.setLastName(dto.getLastName());
		newUser.setAge(dto.getAge());
		newUser.setGender(dto.getGender());
		newUser.setMobileNo(dto.getMobileNo());
		newUser.setSecretQue(dto.getSecretQue());
		newUser.setSecretAns(dto.getSecretAns());
		
		return newUser;
		
	}

}
