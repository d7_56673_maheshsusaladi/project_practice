package com.example.dto;

public class ScheduleDto {

	private int PackageId;

	private int scheduleId;

	private String agenda;

	private String description;

	private int dayNo;

	private int breakfast;

	private int dinner;

	private int lunch;

	public ScheduleDto() {
		super();
	}

	public ScheduleDto(int packageId, int scheduleId, String agenda, String description, int dayNo, int breakfast,
			int dinner, int lunch) {
		super();
		PackageId = packageId;
		this.scheduleId = scheduleId;
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.breakfast = breakfast;
		this.lunch = lunch;
		this.dinner = dinner;
	}

	public ScheduleDto(int scheduleId, String agenda, String description, int dayNo, int breakfast, int dinner,
			int lunch) {
		super();
		this.scheduleId = scheduleId;
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.breakfast = breakfast;
		this.lunch = lunch;
		this.dinner = dinner;
	}

	public ScheduleDto(String agenda, String description, int dayNo, int breakfast, int dinner, int lunch) {
		super();
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.breakfast = breakfast;
		this.lunch = lunch;
		this.dinner = dinner;
	}

	public int getPackageId() {
		return PackageId;
	}

	public void setPackageId(int packageId) {
		PackageId = packageId;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getAgenda() {
		return agenda;
	}

	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDayNo() {
		return dayNo;
	}

	public void setDayNo(int dayNo) {
		this.dayNo = dayNo;
	}

	public int getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(int breakfast) {
		this.breakfast = breakfast;
	}

	public int getDinner() {
		return dinner;
	}

	public void setDinner(int dinner) {
		this.dinner = dinner;
	}

	public int getLunch() {
		return lunch;
	}

	public void setLunch(int lunch) {
		this.lunch = lunch;
	}

	@Override
	public String toString() {
		return "ScheduleDto [PackageId=" + PackageId + ", scheduleId=" + scheduleId + ", agenda=" + agenda
				+ ", description=" + description + ", dayNo=" + dayNo + "]";
	}

}
