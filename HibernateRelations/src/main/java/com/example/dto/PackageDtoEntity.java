package com.example.dto;

import org.springframework.stereotype.Component;

import com.example.entities.Destination;
import com.example.entities.Packagee;
@Component
public class PackageDtoEntity 
{
        public PackageDto toPackageDto(Packagee entity)
        {
        	PackageDto dto = new PackageDto();
        	dto.setPackageId(entity.getPackageId());
        	dto.setPackageName(entity.getPackageName());
        	dto.setPricePerPerson(entity.getPricePerPerson());
        	dto.setCategory(entity.getCategory());
        	dto.setStartDate(entity.getStartDate());
        	dto.setDays(entity.getDays());
        	dto.setDestinationId(entity.getDestination().getDestinationId());
        	dto.setPkgImage(entity.getPkgImage());
        	dto.setTransportMode(entity.getTransportMode().toUpperCase());
        	return dto;
        	
        }
        
        public Packagee toPackageEntity(PackageDto dto)
        {
        	Packagee entity = new Packagee();
        	entity.setPackageId(dto.getPackageId());
        	entity.setPackageName(dto.getPackageName());
        	entity.setPricePerPerson(dto.getPricePerPerson());
        	entity.setCategory(dto.getCategory());
        	entity.setStartDate(dto.getStartDate());
        	entity.setDays(dto.getDays());
        	entity.setPkgImage(dto.getPkgImage());
        	Destination destination = new Destination();
        	destination.setDestinationId(dto.getDestinationId());
        	entity.setDestination(destination);
        	entity.setTransportMode(dto.getTransportMode());
        	return entity;
        }
}
