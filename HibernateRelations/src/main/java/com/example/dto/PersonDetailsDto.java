package com.example.dto;

import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonDetailsDto {

	private int id;
	private String name;
	private int age;
	private String idProof;
	private int bookingId;

	public PersonDetailsDto() {
		super();
	}

	public PersonDetailsDto(String name, int age, String idProof, int bookingId) {
		super();
		this.name = name;
		this.age = age;
		this.idProof = idProof;
		this.bookingId = bookingId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdProof() {
		return idProof;
	}

	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	@Override
	public String toString() {
		return "PersonDetailsDto [id=" + id + ", name=" + name + ", age=" + age + ", idProof=" + idProof
				+ ", bookingId=" + bookingId + "]";
	}
	

}
