package com.example.dto;

import java.util.Date;

public class PackageDto {
	private int packageId;
	private String packageName;
	private double pricePerPerson;
	private String category;
	private Date startDate;
	private int days;
	private int destinationId;
	private String transportMode;
	private String pkgImage;

	public PackageDto() {
		super();
	}

	public PackageDto(int packageId, String packageName, double pricePerPerson, String category, Date startDate,
			int days, int destinationId, String pkgImage, String transportMode) {
		super();
		this.packageId = packageId;
		this.packageName = packageName;
		this.pricePerPerson = pricePerPerson;
		this.category = category;
		this.startDate = startDate;
		this.days = days;
		this.destinationId = destinationId;
		this.pkgImage = pkgImage;
		this.transportMode = transportMode;

	}

	public PackageDto(String packageName, double pricePerPerson, String category, Date startDate, int days,
			int destinationId, String pkgImage) {
		super();
		this.packageName = packageName;
		this.pricePerPerson = pricePerPerson;
		this.category = category;
		this.startDate = startDate;
		this.days = days;
		this.destinationId = destinationId;
		this.pkgImage = pkgImage;
	}

	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public double getPricePerPerson() {
		return pricePerPerson;
	}

	public void setPricePerPerson(double pricePerPerson) {
		this.pricePerPerson = pricePerPerson;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}

	public String getPkgImage() {
		return pkgImage;
	}

	public void setPkgImage(String pkgImage) {
		this.pkgImage = pkgImage;
	}

	public String getTransportMode() {
		return transportMode;
	}

	public void setTransportMode(String transportMode) {
		this.transportMode = transportMode;
	}

	@Override
	public String toString() {
		return "PackageDto [packageId=" + packageId + ", packageName=" + packageName + ", pricePerPerson="
				+ pricePerPerson + ", category=" + category + ", startDate=" + startDate + ", days=" + days
				+ ", destinationId=" + destinationId + ", transportMode=" + transportMode + ", pkgImage=" + pkgImage
				+ "]";
	}

}
