package com.example.dto;

public class QuestionCredential {

	private int userId;
	private String email;
	private String secretQue;
	private String secretAns;
	
	public QuestionCredential(int userId, String email, String secretQue, String secretAns) {
		super();
		this.userId = userId;
		this.email = email;
		this.secretQue = secretQue;
		this.secretAns = secretAns;
	}

	public QuestionCredential() {
		super();
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecretQue() {
		return secretQue;
	}

	public void setSecretQue(String secretQue) {
		this.secretQue = secretQue;
	}

	public String getSecretAns() {
		return secretAns;
	}

	public void setSecretAns(String secretAns) {
		this.secretAns = secretAns;
	}

	@Override
	public String toString() {
		return "QuestionCredential [userId=" + userId + ", email=" + email + ", secretQue=" + secretQue + ", secretAns="
				+ secretAns + "]";
	}
	
	
	
	
}
