package com.example.dto;

import org.springframework.stereotype.Component;

import com.example.entities.Booking;
import com.example.entities.PersonDetails;

@Component
public class PersonDtoEntity {

	public PersonDetailsDto toPersonDetailsDto(PersonDetails entity) {
		PersonDetailsDto dto = new PersonDetailsDto();
		dto.setBookingId(entity.getBooking().getBookingId());
		dto.setName(entity.getName());
		dto.setAge(entity.getAge());
		dto.setIdProof(entity.getIdProof());
		return dto;
	}

	public PersonDetails toPersonDetailsEntity(PersonDetailsDto dto) {
		PersonDetails entity = new PersonDetails();
		entity.setName(dto.getName());
		entity.setAge(dto.getAge());
		entity.setIdProof(dto.getIdProof());
		Booking newBooking = new Booking();
		newBooking.setBookingId(dto.getBookingId());
		entity.setBooking(newBooking);
		return entity;
	}
}
