package com.example.dto;

import java.util.Date;

public class BookingDto {

	private int bookingId;
	private int personCount;
	private double totalPrice;
	private String idProofLink;
	private int userId;
	private int packageId;
	private Date bookingDate;
	private String bookingPdfLink;
	

	public BookingDto() {
		
	}

	public BookingDto(int bookingId, int personCount, double totalPrice, String idProofLink, int userId, int packageId,
			Date bookingDate, String bookingPdfLink) {
		super();
		this.bookingId = bookingId;
		this.personCount = personCount;
		this.totalPrice = totalPrice;
		this.idProofLink = idProofLink;
		this.userId = userId;
		this.packageId = packageId;
		this.bookingDate = bookingDate;
		this.bookingPdfLink = bookingPdfLink;
		
	}



	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public int getPersonCount() {
		return personCount;
	}

	public void setPersonCount(int personCount) {
		this.personCount = personCount;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getIdProofLink() {
		return idProofLink;
	}

	public void setIdProofLink(String idProofLink) {
		this.idProofLink = idProofLink;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	
	public String getBookingPdfLink() {
		return bookingPdfLink;
	}

	public void setBookingPdfLink(String bookingPdfLink) {
		this.bookingPdfLink = bookingPdfLink;
	}

	@Override
	public String toString() {
		return "BookingDto [bookingId=" + bookingId + ", personCount=" + personCount + ", totalPrice=" + totalPrice
				+ ", idProofLink=" + idProofLink + ", userId=" + userId + ", packageId=" + packageId + ", bookingDate="
				+ bookingDate + ", bookingPdfLink=" + bookingPdfLink + "]";
	}

	

}
