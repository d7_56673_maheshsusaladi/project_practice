package com.example.dto;

import org.springframework.stereotype.Component;

import com.example.entities.Destination;

@Component
public class DestinationDtoEntity {
	public DestinationDto toDestinationDto(Destination entity) {
		DestinationDto dto = new DestinationDto();
		dto.setDestinationId(entity.getDestinationId());
		dto.setStateName(entity.getStateName());
		dto.setStateInfo(entity.getStateInfo());
		dto.setCountryName(entity.getCountryName());
		dto.setCountryInfo(entity.getCountryInfo());
		dto.setDestImage(entity.getDestImage());
		return dto;
	}

	public Destination toDestinationEntity(DestinationDto dto) {
		Destination entity = new Destination();
		entity.setDestinationId(dto.getDestinationId());
		entity.setStateName(dto.getStateName());
		entity.setStateInfo(dto.getStateInfo());
		entity.setCountryName(dto.getCountryName());
		entity.setCountryInfo(dto.getCountryInfo());
		entity.setDestImage(dto.getDestImage());
		return entity;
	}
}
