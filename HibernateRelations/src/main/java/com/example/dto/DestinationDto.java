package com.example.dto;

public class DestinationDto {
	private int destinationId;
	private String stateName;
	private String stateInfo;
	private String countryName;
	private String countryInfo;
	private String destImage;
	

	public DestinationDto() {
		super();
	}

	public DestinationDto(int destinationId, String stateName, String stateInfo, String countryName, String countryInfo,
			String destImage, String transportMode) {
		super();
		this.destinationId = destinationId;
		this.stateName = stateName;
		this.stateInfo = stateInfo;
		this.countryName = countryName;
		this.countryInfo = countryInfo;
		this.destImage = destImage;
		
	}

	public DestinationDto(String stateInfo, String destImage) {
		super();
		this.stateInfo = stateInfo;
		this.destImage = destImage;
	}

	public int getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryInfo() {
		return countryInfo;
	}

	public void setCountryInfo(String countryInfo) {
		this.countryInfo = countryInfo;
	}

	public String getDestImage() {
		return destImage;
	}

	public void setDestImage(String destImage) {
		this.destImage = destImage;
	}

	

	@Override
	public String toString() {
		return "DestinationDto [destinationId=" + destinationId + ", stateName=" + stateName + ", stateInfo="
				+ stateInfo + ", countryName=" + countryName + ", countryInfo=" + countryInfo + ", destImage="
				+ destImage + "]";
	}

}
