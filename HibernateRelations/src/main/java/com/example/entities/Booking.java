package com.example.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "booking")
public class Booking {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column
	private int bookingId;
	@Column
	private int personCount;
	@Column
	private double totalPrice;
	@Column
	private String idProofLink;
	@Column
	private String bookingPdfLink;

	@Column
	@Temporal(TemporalType.DATE)
	private Date bookingDate;

	@ManyToOne()
	@JoinColumn(name = "userId")
	private User user;

	@ManyToOne()
	@JoinColumn(name = "packageId")
	private Packagee packagee;

	@OneToMany(mappedBy = "booking",cascade = {CascadeType.REMOVE, CascadeType.PERSIST })	
	private List<PersonDetails> person;
	
	public Booking() {
		super();
	}


	public Booking(int bookingId, int personCount, double totalPrice, String idProofLink, String bookingPdfLink,
			Date bookingDate, User user, Packagee packagee, List<PersonDetails> person) {
		super();
		this.bookingId = bookingId;
		this.personCount = personCount;
		this.totalPrice = totalPrice;
		this.idProofLink = idProofLink;
		this.bookingPdfLink = bookingPdfLink;
		this.bookingDate = bookingDate;
		this.user = user;
		this.packagee = packagee;
		this.person = person;
	}

   
	public Booking(int personCount, double totalPrice, String idProofLink, String bookingPdfLink, Date bookingDate,
			User user, Packagee packagee, List<PersonDetails> person) {
		super();
		this.personCount = personCount;
		this.totalPrice = totalPrice;
		this.idProofLink = idProofLink;
		this.bookingPdfLink = bookingPdfLink;
		this.bookingDate = bookingDate;
		this.user = user;
		this.packagee = packagee;
		this.person = person;
	}


	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public int getPersonCount() {
		return personCount;
	}

	public void setPersonCount(int personCount) {
		this.personCount = personCount;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getIdProofLink() {
		return idProofLink;
	}

	public void setIdProofLink(String idProofLink) {
		this.idProofLink = idProofLink;
	}

	public String getBookingPdfLink() {
		return bookingPdfLink;
	}

	public void setBookingPdfLink(String bookingPdfLink) {
		this.bookingPdfLink = bookingPdfLink;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Packagee getPackagee() {
		return packagee;
	}

	public void setPackagee(Packagee packagee) {
		this.packagee = packagee;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public List<PersonDetails> getPerson() {
		return person;
	}


	public void setPerson(List<PersonDetails> person) {
		this.person = person;
	}


	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", personCount=" + personCount + ", totalPrice=" + totalPrice
				+ ", idProofLink=" + idProofLink + ", bookingPdfLink=" + bookingPdfLink + ", bookingDate=" + bookingDate
				+ ", user=" + user + ", packagee=" + packagee + ", person=" + person + "]";
	}


	
}
