package com.example.entities;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "package")
public class Packagee {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column
	private int packageId;
	@Column
	private String packageName;
	@Column
	private double pricePerPerson;
	@Column
	private String category;
	@Column
	@Temporal(TemporalType.DATE)
	private Date startDate;
	@Column
	private int days;
	@Column
	private String pkgImage;

	@Column
	private String transportMode;

	@ManyToOne()
	@JoinColumn(name = "destinationId")
	@JsonIgnore
	private Destination destination;

	@OneToMany(mappedBy = "packagee", cascade = { CascadeType.REMOVE })
	private List<Booking> bookings;

	@OneToMany(mappedBy = "packagee", cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
	private List<DayWiseSchedule> schedule;

	public Packagee() {
		super();
	}

	public Packagee(int packageId, String packageName, double pricePerPerson, String category, Date startDate, int days,

			String pkgImage, String transportMode) {

		super();
		this.packageId = packageId;
		this.packageName = packageName;
		this.pricePerPerson = pricePerPerson;
		this.category = category;
		this.startDate = startDate;
		this.days = days;
		this.pkgImage = pkgImage;
		this.transportMode = transportMode;

	}

	public Packagee(String packageName, double pricePerPerson, String category, Date startDate, int days,
			String pkgImage) {
		super();
		this.packageName = packageName;
		this.pricePerPerson = pricePerPerson;
		this.category = category;
		this.startDate = startDate;
		this.days = days;
		this.pkgImage = pkgImage;
	}

	public Packagee(int packageId, String packageName, double pricePerPerson, String category, Date startDate, int days,
			String pkgImage, Destination destination, List<Booking> bookings) {
		super();
		this.packageId = packageId;
		this.packageName = packageName;
		this.pricePerPerson = pricePerPerson;
		this.category = category;
		this.startDate = startDate;
		this.days = days;
		this.pkgImage = pkgImage;
		this.destination = destination;
		this.bookings = bookings;
	}

	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public double getPricePerPerson() {
		return pricePerPerson;
	}

	public void setPricePerPerson(double pricePerPerson) {
		this.pricePerPerson = pricePerPerson;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public String getPkgImage() {
		return pkgImage;
	}

	public void setPkgImage(String pkgImage) {
		this.pkgImage = pkgImage;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public List<DayWiseSchedule> getSchedule() {
		return schedule;
	}

	public void setSchedule(List<DayWiseSchedule> schedule) {
		this.schedule = schedule;
	}

	public void addPackage(DayWiseSchedule schedule) {
		this.schedule.add(schedule);
		schedule.setPackagee(this);
	}

	public String getTransportMode() {
		return transportMode;
	}

	public void setTransportMode(String transportMode) {
		this.transportMode = transportMode;
	}

	@Override
	public String toString() {
		return "Packagee [packageId=" + packageId + ", packageName=" + packageName + ", pricePerPerson="
				+ pricePerPerson + ", category=" + category + ", startDate=" + startDate + ", days=" + days
				+ ", pkgImage=" + pkgImage + ", transportMode=" + transportMode + ", destination=" + destination
				+ ", bookings=" + bookings + ", schedule=" + schedule + "]";
	}

}
