package com.example.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "destination")
public class Destination {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column
	private int destinationId;
	@Column
	private String stateName;
	@Column
	private String stateInfo;
	@Column
	private String countryName;
	@Column
	private String countryInfo;
	@Column
	private String destImage;

	@OneToMany(mappedBy = "destination", cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
	private List<Packagee> packageList;

	public Destination() {
		super();
	}

	public Destination(int destinationId, String stateName, String stateInfo, String countryName, String countryInfo,
			List<Packagee> packageList, String destImage) {
		super();
		this.destinationId = destinationId;
		this.stateName = stateName;
		this.stateInfo = stateInfo;
		this.countryName = countryName;
		this.countryInfo = countryInfo;
		this.packageList = packageList;
		this.destImage = destImage;
	}

	public Destination(String stateName, String stateInfo, String countryName, String countryInfo,
			List<Packagee> packageList, String destImage) {
		super();
		this.stateName = stateName;
		this.stateInfo = stateInfo;
		this.countryName = countryName;
		this.countryInfo = countryInfo;
		this.packageList = packageList;
		this.destImage = destImage;
	}

	public int getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryInfo() {
		return countryInfo;
	}

	public void setCountryInfo(String countryInfo) {
		this.countryInfo = countryInfo;
	}

	public List<Packagee> getPackageList() {
		return packageList;
	}

	public void setPackageList(List<Packagee> packageList) {
		this.packageList = packageList;
	}

	public String getDestImage() {
		return destImage;
	}

	public void setDestImage(String destImage) {
		this.destImage = destImage;
	}

	@Override
	public String toString() {
		return "Destination [destinationId=" + destinationId + ", stateName=" + stateName + ", stateInfo=" + stateInfo
				+ ", countryName=" + countryName + ", countryInfo=" + countryInfo + ", destImage=" + destImage
				+ ", packageList=" + packageList + "]";
	}

	public void addNewPackage(Packagee newPackage) {
		if (this.packageList == null) { // if new Destination is to be inserted
			this.packageList = new ArrayList<>();
		}
		int index = this.packageList.indexOf(newPackage);
		if (index == -1) // empty list with index of package
		{
			this.packageList.add(newPackage);
		} else { // there is already package present
			this.packageList.set(index, newPackage);
		}
	}

}
