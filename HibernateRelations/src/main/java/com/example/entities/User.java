package com.example.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column
	private int userId;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private String email;
	@Column
	private String password;
	@Column
	private int age;
	@Column
	private String gender;
	@Column
	private String mobileNo;
	@Column
	private String secretQue;
	@Column
	private String secretAns;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<Booking> userBookings;

	@Column
	private String role;

	public User() {
		super();
	}

	public User(String firstName, String lastName, String email, String password, int age, String gender,
			String mobileNo, String secretQue, String secretAns, String role) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.age = age;
		this.gender = gender;
		this.mobileNo = mobileNo;
		this.secretQue = secretQue;
		this.secretAns = secretAns;
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSecretQue() {
		return secretQue;
	}

	public void setSecretQue(String secretQue) {
		this.secretQue = secretQue;
	}

	public String getSecretAns() {
		return secretAns;
	}

	public void setSecretAns(String secretAns) {
		this.secretAns = secretAns;
	}

	public List<Booking> getUserBookings() {
		return userBookings;
	}

	public void setUserBookings(List<Booking> userBookings) {
		this.userBookings = userBookings;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", age=" + age + ", gender=" + gender + ", mobileNo=" + mobileNo
				+ ", secretQue=" + secretQue + ", secretAns=" + secretAns + ", userBookings=" + userBookings + ", role="
				+ role + "]";
	}

}