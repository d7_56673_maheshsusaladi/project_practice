package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="personDetails")
public class PersonDetails {
	
//	id | name             | age  | idProof  | bookingId

	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	@Id
	private int id;
	@Column
	private String name;
	@Column
	private int age;
	@Column
	private String idProof;
	@ManyToOne()
	@JoinColumn(name = "bookingId")
	private Booking booking;
		
	public PersonDetails() {
		super();
	}

	public PersonDetails(int id, String name, int age, String idProof,Booking booking) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.idProof = idProof;
		
		this.booking = booking;
	}

	public PersonDetails(String name, int age, String idProof,Booking booking) {
		super();
		this.name = name;
		this.age = age;
		this.idProof = idProof;	
		this.booking = booking;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdProof() {
		return idProof;
	}

	public void setIdProof(String idProof) {
		this.idProof = idProof;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	@Override
	public String toString() 
	{
		return "PersonDetails [id=" + id + ", name=" + name + ", age=" + age + ", idProof=" + idProof + ", booking="
				+ booking + "]";
	}

}
