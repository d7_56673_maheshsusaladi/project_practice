package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dayWiseSchedule")
public class DayWiseSchedule {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int scheduleId;
	@Column
	private String agenda;
	@Column
	private String description;
	@Column
	private int dayNo;

	@ManyToOne
	@JoinColumn(name = "packageId")
	private Packagee packagee;
	@Column
	private int breakfast;
	@Column
	private int dinner;
	@Column
	private int lunch;

	public DayWiseSchedule() {
		super();
	}

	public DayWiseSchedule(String agenda, String description, int dayNo, Packagee packagee) {
		super();
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.packagee = packagee;
	}

	public DayWiseSchedule(int scheduleId, String agenda, String description, int dayNo, int breakfast, int dinner,
			int lunch) {
		super();
		this.scheduleId = scheduleId;
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.breakfast = breakfast;
		this.lunch = lunch;
		this.dinner = dinner;
	}

	public DayWiseSchedule(int scheduleId, String agenda, String description, int dayNo, Packagee packagee) {
		super();
		this.scheduleId = scheduleId;
		this.agenda = agenda;
		this.description = description;
		this.dayNo = dayNo;
		this.packagee = packagee;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getAgenda() {
		return agenda;
	}

	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDayNo() {
		return dayNo;
	}

	public void setDayNo(int dayNo) {
		this.dayNo = dayNo;
	}

	public Packagee getPackagee() {
		return packagee;
	}

	public void setPackagee(Packagee packagee) {
		this.packagee = packagee;
	}

	public int getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(int breakfast) {
		this.breakfast = breakfast;
	}

	public int getDinner() {
		return dinner;
	}

	public void setDinner(int dinner) {
		this.dinner = dinner;
	}

	public int getLunch() {
		return lunch;
	}

	public void setLunch(int lunch) {
		this.lunch = lunch;
	}

	@Override
	public String toString() {
		return "DayWiseSchedule [scheduleId=" + scheduleId + ", agenda=" + agenda + ", description=" + description
				+ ", dayNo=" + dayNo + "]";
	}

}
