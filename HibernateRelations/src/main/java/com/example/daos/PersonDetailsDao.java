package com.example.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.PersonDetails;

public interface PersonDetailsDao extends JpaRepository<PersonDetails, Integer>{


}
