package com.example.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Destination;

public interface DestinationDao extends JpaRepository<Destination, Integer> {

	Destination findByDestinationId(int destinationId);

	Destination findDestinationByStateName(String stateName);

	List<Destination> findDestinationByCountryName(String countryName);

}
