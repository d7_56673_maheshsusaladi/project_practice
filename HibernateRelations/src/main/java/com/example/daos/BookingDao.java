package com.example.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Booking;

public interface BookingDao extends JpaRepository<Booking, Integer>{

}
