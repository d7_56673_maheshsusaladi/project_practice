package com.example.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Packagee;

public interface PackageDao extends JpaRepository<Packagee, Integer> {

	Packagee findByPackageId(int id);
	
	List<Packagee> findPackageeByPackageName(String packagename);

}
