package com.example.daos;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.entities.User;

public interface UserDao extends JpaRepository<User, Integer>{

	User findByEmailAndPassword(String email,String password);
	
	User findByEmail(String email);
	
	User findByUserId(int id);
	
	@Query(value = "select p.packageName,b.totalPrice,b.bookingdate,b.personcount from booking b "
			+ "INNER JOIN user u ON u.userId = b.userId "
			+ "INNER JOIN package p ON p.packageId = b.packageId where u.userId=? order by b.bookingdate desc;", nativeQuery = true)
	List<Object>  findUserHistoryById(int id);
}
