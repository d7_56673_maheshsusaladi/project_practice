package com.example.daos;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.entities.DayWiseSchedule;

public interface DayWiseScheduleDao extends JpaRepository<DayWiseSchedule, Integer>{

	DayWiseSchedule findByScheduleId(int id);
	
	
}
