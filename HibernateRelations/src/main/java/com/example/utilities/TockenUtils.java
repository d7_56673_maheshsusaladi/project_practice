package com.example.utilities;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TockenUtils {

	@Autowired
	JwtUtil jwtUtil; 
	
	public  String getIdFromTocken(HttpServletRequest request)
	{		String token= request.getHeader("authorization");
		 	String[] chunks = token.split("\\.");
			Base64.Decoder decoder = Base64.getUrlDecoder();
			String payload = new String(decoder.decode(chunks[1]));
			JSONObject json = new JSONObject(payload);
			String id = json.getString("id"); 
			System.out.println(json);
		return id;
	}
	
	
		
	
}
