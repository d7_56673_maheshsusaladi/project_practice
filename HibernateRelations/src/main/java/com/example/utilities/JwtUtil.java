package com.example.utilities;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.example.autheticationObject.MyUserDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {
//----------variables-------------------------------------------------------------------------
	@Autowired
	private Environment env;
//-----------request mapping methods----------------------------------------------------------	

	public String generateToken(MyUserDetails user) {
		Map<String, Object> info = new HashMap<String, Object>();
		String authorities = userAuthorities(user);
		info.put("authorities", authorities);
		info.put("id", user.getUsername());
		return createToken(info);

	}

	private String createToken(Map<String, Object> info) {
		long curTime = System.currentTimeMillis();
		long expiration = env.getProperty("jwt.token.expiration.millis", Long.class);
		String secret = env.getProperty("jwt.token.secret");
		return Jwts.builder().setSubject(null).setClaims(info).setIssuedAt(new Date(curTime))
				.setExpiration(new Date(curTime + expiration)).signWith(SignatureAlgorithm.HS256, secret).compact();
	}

	public boolean validateToken(String token, UserDetails user) {
		Claims claims = decodeToken(token);
		if (!claims.get("id").equals(user.getUsername()))
			return false;
		if (!claims.get("authorities").equals(userAuthorities(user)))
			return false;
		if (claims.getExpiration().before(new Date()))
			return false;
		return true;
	}

	private Claims decodeToken(String token) {
		String secret = env.getProperty("jwt.token.secret");
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	public String extractUsername(String token) {
		Claims claims = decodeToken(token);
		String email = claims.get("id", String.class);
		return email;
	}

	private String userAuthorities(UserDetails user) {
		return user.getAuthorities().stream().map(authority -> authority.getAuthority())
				.collect(Collectors.joining(","));
	}

}// end of class
