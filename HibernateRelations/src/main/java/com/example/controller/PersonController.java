package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.PersonDetailsDto;
import com.example.dto.Response;
import com.example.entities.PersonDetails;
import com.example.service.PersonDetailsService;
@CrossOrigin
@RestController
public class PersonController {

	@Autowired
	PersonDetailsService service;
	
	@PostMapping("/user/person/add")
	public ResponseEntity<?> addPersonDetails(@RequestBody PersonDetailsDto dto) {
		PersonDetails person=service.addPersonDetails(dto);
		if(person!=null)
			return Response.success(person);
		return Response.error("Add Failed!");
		
	}

//	@PostMapping("/personDetails/add")
//	public ResponseEntity<?> addPersonDetails(@RequestBody ArrayList<PersonDetailsDto> list) {
//		return null;
//		
//	}
}
