package com.example.controller;

import static com.example.dto.Response.error;
import static com.example.dto.Response.success;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.dto.DestinationDto;
import com.example.dto.PackageDto;
import com.example.dto.Response;
import com.example.service.DestinationService;

@CrossOrigin
@RestController
public class DestinationController {

	@Autowired
	private DestinationService service;

	@Autowired

	// Get all destinations
	@GetMapping("/home/destinations")
	public ResponseEntity<?> getAllDestinations() {
		List<DestinationDto> allDestinations = service.getAllDestinations();
		if (allDestinations != null)
			return success(allDestinations);
		return error(null);
	}

	// Get packages By destinationId
	@GetMapping("/home/destination/{id}")
	public ResponseEntity<?> getPackagesByDestinationId(@PathVariable("id") int destId) {
		List<PackageDto> packages = service.getById(destId);
		if (packages != null)
			return success(packages);
		return error(null);
	}

	// Upload destination image
	@PostMapping(value = "/admin/image/upload/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadPackagefile(@RequestParam("datafile") MultipartFile file, @PathVariable("id") int id)
			throws IOException {
		Map<String, Object> uploadImage = service.uploadImage(file, id);
		return Response.success(uploadImage);
	}

	// Display destination image
	@GetMapping(value = "/home/image/download/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<ByteArrayResource> downloadImage(@PathVariable("id") int id) {
		try {

			ByteArrayResource resource = service.downloadPhoto(id);
			return ResponseEntity.status(HttpStatus.OK).contentLength(resource.contentLength()).body(resource);

		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

}
