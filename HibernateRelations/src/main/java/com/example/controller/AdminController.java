package com.example.controller;

import static com.example.dto.Response.error;
import static com.example.dto.Response.success;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.dto.DestinationDto;
import com.example.dto.PackageDto;
import com.example.dto.Response;
import com.example.dto.ScheduleDto;
import com.example.service.DestinationService;
import com.example.service.PackageService;
import com.example.service.ScheduleService;

@CrossOrigin
@RestController

//@CrossOrigin(origins = "*")

public class AdminController {

	@Autowired
	PackageService packageService;
	@Autowired
	DestinationService destinationService;

	@Autowired
	ScheduleService scheduleService;

	@PostMapping("/admin/add_destination")
	public ResponseEntity<?> addDestination(@RequestBody DestinationDto destination) {
		System.out.println(destination);
		DestinationDto addedDestination = destinationService.addNewDestination(destination);
		if (addedDestination != null) {
			return Response.success(addedDestination);
		}

		return Response.error("failed");
	}

	@DeleteMapping("/admin/delete_destination/{id}")
	public ResponseEntity<?> deleteDesitnation(@PathVariable("id") int id) {
		String result = destinationService.deleteDestination(id);
		if (result != null) {
			return Response.success(result);
		}

		return Response.error("failed to delete");
	}

	@PostMapping("admin/edit_destination")
	public ResponseEntity<?> editDestination(@RequestBody DestinationDto destination) {
		String result = destinationService.updateDestination(destination);
		if (result != null) {
			return Response.success(result);
		}

		return Response.error("Update failed");
	}

	@GetMapping("/admin/destinations")
	public ResponseEntity<?> getaAllDestination() {
		List<DestinationDto> allDestinations = destinationService.getAllDestinations();

		if (allDestinations.isEmpty()) {
			return Response.error("No destinations available!");
		}
		return Response.success(allDestinations);
	}

	@PostMapping("/admin/add_package")
	public ResponseEntity<?> addPackage(@RequestBody PackageDto dto) {
		PackageDto addNewPackage = packageService.addNewPackage(dto);
		if (addNewPackage != null) {
			return Response.success(addNewPackage);
		}

		return Response.error("failed");
	}

	@DeleteMapping("/admin/package/delete/{id}")
	public ResponseEntity<?> deletePackage(@PathVariable("id") int id) {
		Map<String, Object> result = packageService.deletePackageByid(id);
		if (result != null) {
			return Response.success(result);
		}

		return Response.error("failed to delete");
	}
	
	@GetMapping("/admin/package/schedule/{id}")
	public ResponseEntity<?> getPackageSchedule(@PathVariable("id") int id) {
		List<ScheduleDto> packageSchedule = packageService.getPackageSchedule(id);
		return Response.success(packageSchedule);
	}

	@PostMapping("/admin/edit-package")
	public ResponseEntity<?> editPackage(@RequestBody PackageDto dto) {

		String result = packageService.updatePackage(dto);
		if (result != null) {
			return Response.success(result);
		}

		return Response.error(result);
	}

	@GetMapping("/admin/getall/{id}")
	public ResponseEntity<?> getAllPacakage(@PathVariable("id") int id) {
		List<PackageDto> allPackage = destinationService.getAllPackages(id);
		if (allPackage.isEmpty()) {
			return Response.error("No destinations available!");
		}
		return Response.success(allPackage);
	}

	@PostMapping("admin/add_schedule")
	public ResponseEntity<?> addSchedule(@RequestBody ScheduleDto dto) {
		Map<String, Object> result = scheduleService.addSchedule(dto);
		if (result != null) {
			return Response.success(result);
		}

		return Response.error("failed to add Schedule!");
	}

	@GetMapping("/admin/getpackage/{id}")
	public ResponseEntity<?> getPackage(@PathVariable("id") int id) {
		PackageDto pkg = packageService.findById(id);
		if (pkg != null)
			return Response.success(pkg);
		return Response.error("package not found!");
	}

	@GetMapping("/admin/destination/{id}")
	public ResponseEntity<?> getdByDestinationId(@PathVariable("id") int destId) {
		DestinationDto dto = destinationService.findByDestinationId(destId);
		if (dto != null)
			return success(dto);
		return error(null);
	}

	// Get destinations of particular country
	@GetMapping("/home/destinations/{countryName}")
	public ResponseEntity<?> getByCountryName(@PathVariable("countryName") String countryName) {
		List<DestinationDto> getallDestinationsInCountry = destinationService.getallDestinationsInCountry(countryName);
		if (getallDestinationsInCountry != null)
			return Response.success(getallDestinationsInCountry);
		return Response.error(null);
	}

	

	// Upload package image
	@PostMapping(value = "/admin/image/upload/package/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadPackagefile(@RequestParam("datafile") MultipartFile file, @PathVariable("id") int id)
			throws IOException {
		Map<String, Object> uploadImage = packageService.uploadPackageImage(file, id);
		return Response.success(uploadImage);
	}

	// Display package image
	@GetMapping(value = "/home/image/download/package/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<ByteArrayResource> downloadPackageImage(@PathVariable("id") int id) {
		try {

			ByteArrayResource resource = packageService.downloadPackageImage(id);
			return ResponseEntity.status(HttpStatus.OK).contentLength(resource.contentLength()).body(resource);

		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

}
