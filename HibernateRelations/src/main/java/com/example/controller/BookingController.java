package com.example.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.dto.BookingDto;
import com.example.dto.PersonDetailsDto;
import com.example.dto.Response;
import com.example.service.BookingSevice;

@CrossOrigin
@RestController
public class BookingController {

	@Autowired
	private BookingSevice service;
	
	
	
	@PostMapping("/user/booking/add")
	public ResponseEntity<?> addNewBooking(@RequestBody BookingDto dto)
	{
		
		BookingDto result = service.addNewBooking(dto);
		if(result!=null)
		   return Response.success(result);
		return Response.error("Adding Failed");
	} 
	
	@DeleteMapping("/user/booking/{id}")
	public ResponseEntity<?> deleteBooking(@PathVariable("id") int bookingId)
	{
		Map<String, Object> deleteBooking = service.deleteBooking(bookingId);
		if(!deleteBooking.isEmpty())
			return Response.success(deleteBooking);
		return Response.error("delete failed");
	
	}
	
	@GetMapping("/user/booking/getpersons/{id}") 
	public ResponseEntity<?> getPersons(@PathVariable("id")int id)
	{
		List<PersonDetailsDto> peopleFromBookingId = service.getPeopleFromBookingId(id);
		if(!peopleFromBookingId.isEmpty())
			return Response.success(peopleFromBookingId);
		return Response.error("no person availale!");
			
	}
	
	@GetMapping("/user/booking/getprice/{id}")
	public ResponseEntity<?> getPrice(@PathVariable("id")int id)
	{
		double totalPriceForBooking = service.getTotalPriceForBooking(id);
		
			return Response.success(totalPriceForBooking);
		
			
	} 
 	
}
