package com.example.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.dto.PackageDto;
import com.example.dto.Response;
import com.example.dto.ScheduleDto;
import com.example.service.DestinationService;
import com.example.service.PackageService;

@CrossOrigin
@RestController
public class PackageController {
	@Autowired
	private PackageService service;
	@Autowired
	private DestinationService destiantionService;

//	// Add new package
//	@PostMapping("/package/add")
//	public ResponseEntity<?> addNewPackageC(@RequestBody PackageDto dto) {
//		
//		Map<String, Object> result = service.addNewPackage(dto);
//		return Response.success(result);
//	}

	@GetMapping("/package/{cityName}")
	public ResponseEntity<?> findPackageByName(@PathVariable("cityName") String cityName) {
		List<PackageDto> allPackgesofCity = service.getAllPackgesofCity(cityName);
		return Response.success(allPackgesofCity);
	}

	@DeleteMapping("/package/delete/{id}")
	public ResponseEntity<?> deletePackageByid(@PathVariable("id") int deleteId) {
		Map<String, Object> deletePackageByid = service.deletePackageByid(deleteId);
		return Response.success(deletePackageByid);
	}

	@GetMapping("/package/getall/{id}")
	public ResponseEntity<?> getAllPacakage(@PathVariable("id") int id) {
		List<PackageDto> allPackage = destiantionService.getAllPackages(id);
		if (allPackage.isEmpty()) {
			return Response.error("No destinations available!");
		}
		return Response.success(allPackage);
	}

	@GetMapping("/home/package/schedule/{id}")
	public ResponseEntity<?> getPackageSchedule(@PathVariable("id") int id) {
		List<ScheduleDto> packageSchedule = service.getPackageSchedule(id);
		return Response.success(packageSchedule);
	}
	
	@GetMapping("/home/getpackage/{id}")
	public ResponseEntity<?> getPackage(@PathVariable("id") int id) {
		PackageDto pkg = service.findById(id);
		if (pkg != null)
			return Response.success(pkg);
		return Response.error("package not found!");
	}

}
