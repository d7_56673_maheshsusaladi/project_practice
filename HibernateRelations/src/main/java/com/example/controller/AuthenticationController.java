package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.autheticationObject.MyUserDetails;
import com.example.dto.Credential;
import com.example.utilities.JwtUtil;

@CrossOrigin(origins = "*")
@RequestMapping("/authenticate")
@RestController
public class AuthenticationController {

//----------variables----------------------------------	
	@Autowired
	private AuthenticationManager authManager;
	@Autowired
	private JwtUtil jwtUtils;

//-------------------------------methods-----------------------------------------------------------
	@PostMapping()
	public ResponseEntity<String> authenticate(@RequestBody Credential authenticationRequestDto) {
		try {
			Authentication auth = authManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequestDto.getEmail(), authenticationRequestDto.getPassword()));
			MyUserDetails user = (MyUserDetails) auth.getPrincipal();
			String token = jwtUtils.generateToken(user);
			return ResponseEntity.ok(token);
		} catch (BadCredentialsException e) {
			return ResponseEntity.notFound().build();
		}
	}

}// end of class
