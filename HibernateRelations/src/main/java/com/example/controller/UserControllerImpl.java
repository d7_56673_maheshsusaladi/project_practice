package com.example.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.dto.Credential;
import com.example.dto.QuestionCredential;
import com.example.dto.Response;
import com.example.dto.UserDto;
import com.example.entities.User;
import com.example.service.UserServiceImpl;

@CrossOrigin
@RestController
public class UserControllerImpl {

	@Autowired
	private UserServiceImpl userService;

	@PostMapping("/user/signin")
	public ResponseEntity<?> signIn(@RequestBody Credential cred) {
		UserDto userDto = userService.findByEmailAndPassword(cred);
		if (userDto != null)
			return Response.success(userDto);
		return Response.error("user not found");
	}

	@PostMapping("/home/signup")
	public ResponseEntity<?> signUp(@RequestBody User user) {
		Object result = userService.saveUser(user);

		if (result != null && result instanceof UserDto )
			return Response.success(result);
		return Response.error("signup failed,Email Already Exists! ");
	}
	@PostMapping("/user/forgotpassword")
	public ResponseEntity<?> forgotPassword(@RequestBody QuestionCredential question)
	{
		boolean result=userService.forgotPassword(question);
		if(result==true)
			return Response.success(result);
		return Response.error("Wrong Answer");
		
	}
	@PostMapping("/user/changepassword")
	public ResponseEntity<?> changePassword(@RequestBody Credential cred)
	{
		boolean result=userService.changePassword(cred);
		if(result==true)
			return Response.success("Password Changed!");
		return Response.error("Update Failed Try Again!");
		
	}
	@PostMapping("/user/updateprofile")
	public ResponseEntity<?> updateProfile(@RequestBody User user)
	{
		UserDto result=userService.updateProfile(user);
		if(result !=null)
			return Response.success(result);
		return Response.error("Profile Update failed! ");
		
	}
	
//	@GetMapping("user/history/{id}")
//	public ResponseEntity<?> userBookingHistory(@PathVariable("id") int userId) {
//		List<BookingDto> userBookingHistory = userService.userBookingHistory(userId);
//		return Response.success(userBookingHistory);
//	}
	
	@GetMapping("/user/profile/{id}")
	public ResponseEntity<?> findUserInfoBYId(@PathVariable("id")  int id) {
		Map<String, Object> result = userService.findUserInfoBYId(id);
		return Response.success(result);
	}
	
	@GetMapping("/user/history/{id}")
	public ResponseEntity<?> findUserHistoryBYId(@PathVariable("id")  int id) {
		Map<String, Object> result = userService.findUserHistoryById(id);
		return Response.success(result);
	}
}
