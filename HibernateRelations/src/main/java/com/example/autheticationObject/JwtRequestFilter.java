package com.example.autheticationObject;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.utilities.JwtUtil;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private MyUserDetailsService myUserDeailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		System.out.println("in filter");
		String username = null;
		String tocken = null;
		
		// Step 1: Get the authorization header from Request Header
		String authorizationHeader = request.getHeader("authorization");

		// Step 2: Check the request has authorization header, if yes then check if it starts
		// with "Bearer "
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			// Extract JWT token from Authorization Header
			tocken = authorizationHeader.substring(7); // Remove 'Bearer' i.e. first 7 char. from string
			username = jwtUtil.extractUsername(tocken);

		}

		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = myUserDeailsService.loadUserByUsername(username); // this userdetails object is
																						// taken from database
																						// depending on user is officer
																						// or regular user
			boolean validationResult = jwtUtil.validateToken(tocken, userDetails);

			if (validationResult) {
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetails, null,
						userDetails.getAuthorities());
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}
		// pre-processing
		filterChain.doFilter(request, response); // call next filter in the chain
		// post-processing

	}

}// end of class
