package com.example;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;

import com.example.dto.DestinationDto;
import com.example.entities.Destination;
import com.example.entities.Packagee;
import com.example.service.DestinationService;

@Transactional
@Service
@SpringBootTest
public class DestinationTest {

	@Autowired
	private DestinationService destinationService;

	@Test
	public void testgetDestination() {
		destinationService.getById(1).forEach(System.out::println);
	}

	@Test
//	@Rollback(value = false)
	public void testDelete() {
		destinationService.deleteDestination(7);
	}

//	@Test
//	@Rollback(value = false)
//	public void testaddDestination() {
//		Packagee newPackage = new Packagee(0, "Statue of Unity", 5000, "Solo", Date.valueOf("2020-10-05"), 3,
//				"http://www.google.com");
//		List<Packagee> newList = new ArrayList<Packagee>();
//		newList.add(newPackage);
//		destinationService.addNewDestination(
//				new Destination(0, "Gujrat", "State of culture", "India", "Amazing Country", newList, "gujrat.com"));
//	}

	@Test
	public void testgetDestinationByCountryName() {
		destinationService.getallDestinationsInCountry("India").forEach(System.out::println);
	}

	@Test
	void testgetAllpackges() {
		destinationService.getById(1).forEach(System.out::println);
	}

	@Test
	void testgetAllDestinations() {
		destinationService.getAllDestinations();
	}

	@Test
	void testgetPackagesByState() {
		destinationService.getPackagesInState("Maharashtra").forEach(System.out::println);
	}

	// All tests are done and checked. Rev00
}
