package com.example;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;

import com.example.dto.PersonDetailsDto;
import com.example.service.PersonDetailsService;


@Transactional
@Service
@SpringBootTest
public class PersonTest {

	@Autowired
	private PersonDetailsService service;
	
	@Test
	@Rollback(false)
	void addPerson()
	{
		service.addPersonDetails(new PersonDetailsDto("mahesh", 24, "adhar", 1));
	}

	
}