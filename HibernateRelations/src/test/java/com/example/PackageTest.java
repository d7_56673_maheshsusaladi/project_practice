package com.example;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.sql.Date;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import com.example.daos.PackageDao;
import com.example.dto.PackageDto;
import com.example.entities.Packagee;
import com.example.service.PackageService;

@Transactional
@Service
@SpringBootTest
public class PackageTest {

	@Autowired
	private PackageService packageService;
	@Autowired
	private PackageDao daopackage;

	@Test
	void testFind() {
		PackageDto findById = packageService.findById(4);
		System.out.println(findById);
	}


	@Test
//	@Rollback(value = false)
	void testDeletePackage() {
		System.out.println(packageService.deletePackageByid(17));
	}

	@Test
//	@Rollback(value = false)
	void testUpdatePackage() {
		PackageDto tobeUpdated = packageService.findById(3);
		tobeUpdated.setPkgImage("Image updated");
		tobeUpdated.setStartDate(Date.valueOf("2022-03-30"));
		tobeUpdated.setPricePerPerson(900);
		System.out.println(packageService.updatePackage(tobeUpdated));
	}
	@Test
	void testgetPackageByCityName()
	{
		packageService.getAllPackgesofCity("Nagpur").forEach(System.out::println);
	}
	
	@Test
	void testfindByPackageId() {
		Packagee findByPackageId = daopackage.findById(50).orElse(null);
		assertNotNull(findByPackageId);
	}
	
	// All tests are done and checked. Rev00
}
