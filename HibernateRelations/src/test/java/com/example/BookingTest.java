package com.example;

import java.util.Map;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;

import com.example.daos.PackageDao;
import com.example.daos.UserDao;
import com.example.service.BookingSevice;

@Service
@Transactional
@SpringBootTest
public class BookingTest {

	@Autowired
	private BookingSevice bookingService;
	@Autowired
	private UserDao userdao;
	@Autowired
	private PackageDao packageDao;

//	@Test
////	@Rollback(value = false)
//	public void testBookingDelete() {
//		String deleteBooking = bookingService.deleteBooking(24);
//		System.out.println(deleteBooking);
//	}

//	@Test
////	@Rollback(value=false)
//	void testAddBooking() {
//		User user = userdao.findById(5).orElse(null);
//		Packagee packages = packageDao.findById(4).orElse(null);
//		bookingService.addNewBooking(new Booking(0, 5, 5000, "abc.com", "def.com", user, packages));
//	}
//	// All tests are done and checked. Rev00

//	@Test
//	void testPersonFromBookingId() {
//		List<PersonDetailsDto> peopleFromBookingId = bookingService.getPeopleFromBookingId(2);
//		peopleFromBookingId.forEach(System.out::println);
//	}

	@Test
	@Rollback(value = false)
	void testPricePerPerson() {
		double priceperperson = bookingService.getTotalPriceForBooking(51);
		System.out.println(priceperperson);
	}

	@Test
	@Rollback(value = false)
	void testDeleteBooking() {
		Map<String, Object> deleteBooking = bookingService.deleteBooking(42);
		System.out.println(deleteBooking.toString());
	}

}
