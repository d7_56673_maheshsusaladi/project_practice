### User (correction-*adding all response and request body to urls*)
1. /user/sigin -> signin in*
2. /user/signup -> new user sign up*
3. /delete/{id} -> delete user by id*
4. /update -> update user via request body*
5. /forgotpasssword -> new password using Request Body via secrete question*
6. /changepassword -> change password using request body*
7. /profile/{id} -> display booking history of user*

### Admin
1. /user/signin -> admin signin in via role == admin.
2.add destination*
3.delete destination*
4.edit desitnation*
====
5.add package*
6.edit package*
7.delete package*
8.add schedule*


#### destination:
1. /addDestination -> add new dest*
2. /deleteDestination/{id} -> delete dest 
3. /show all -> display all destination* 
4. /updateDestination/{id}  -> update stateInfo, image of Destination*
5. /destination/{countryName} -> packages in particular package*

#### packages
1. /addpackage -> add new pkg*
2. /deletepackage/{id} -> delete pkg*
3. /showall -> display all packages*
4. /updatepackage/{id}  -> update date, image, description, title, price of pkg*
5. /destination/{stateName} -> details of particalur packages in state*
6. /package/{name} -> details of particular package by its name*
7. /allPackages/{id} -> all packages of particular destination*


### Daywise Schedule
1. /addSchedule/{pkgId} -> add daywise schedule for particular package
2. /updateSchedule/{pkgId}/{dayNo} -> update schedule
3. /deleteSchedule/{pkgId}/{dayNo} -> delete daywise schedule

### Booking:
1. /bookpackage/{pkgid} -> book particular package*
2. /cancelpackage/{pkgid} -> cancel particular package* 
3. /history/{usrid} -> get user booking history*


